<?php
check_is_platinum($post->ID);

//set these for the 'tr_article_list' widget. can remain empty.
$cats_for_widget = array();
$post_ids_for_widget = array($post->ID);

add_action('wp_head', 'tr_add_og_tags');
function tr_add_og_tags(){
	$img = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'full'); ?>
	<meta property="og:image" content="<?php echo $img[0]; ?>" />
	<?php
	$og_desc = get_post_meta($post->ID, '_su_description', true);
	$og_desc = $og_desc ? $og_desc : get_the_excerpt($post->ID);
	?>
	<meta property="og:description" content="<?php echo $og_desc; ?>" />
<?php }

get_header();

if ( have_posts() ) while ( have_posts() ) { the_post();
?>
<img src="<?php echo get_stylesheet_directory_uri(); ?>/includes/img/the-resident-logo.svg" id="print-logo" alt="The Resident">
<?php

$layout_option = get_post_meta($post->ID,'layout_option',true);
if(!$layout_option) $layout_option = 'a';

 if ( has_post_thumbnail() and $layout_option == 'a') {
	 $featured_image = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID),'tr-feature');
	 echo '<div class="featured-image" style="background-image:url('.$featured_image[0] .')"><img src="'.$featured_image[0].'"></div>';
 }

?>

<div class="post-heading <?php
	echo has_post_thumbnail()? ' has_post_thumbnail':' no_post_thumbnail';
	if(has_post_thumbnail() and $layout_option) {
		echo ' layout_option_'.$layout_option;
	} else echo ' layout_option_c';
	?>">

	<div class="terms"><?php

		$is_ad_feature = get_post_meta($post->ID,'is_ad_feature',true);
		if($is_ad_feature) echo '<span class="is_ad_feature">AD FEATURE</span> ';

		$cats = get_the_terms($post->ID, 'category');
		if($cats) foreach($cats as $cat) {
			$cats_for_widget[] = $cat->term_id;
			echo '<a class="category" href="'.get_term_link($cat).'">'.$cat->name.'</a> ';
		}
	?></div>

	 <h1><?php the_title(); ?></h1>

	 <div class="post-meta">
		 <?php

		 $the_author = get_the_terms( get_the_ID(), 'the_author' );

		 if($the_author) foreach($the_author as $author) echo '<a class="tag" href="'.get_term_link($author).'">'.$author->name.'</a> ';
		 else echo 'Posted ';
		  ?>
		on <strong><?php the_date(); ?></strong> <?php
		$tags = get_the_terms($post->ID, 'post_tag');
		if($tags) {
			echo '<span class="vertical-line"></span> Tagged ';
			foreach($tags as $tag) echo '<a class="tag" href="'.get_term_link($tag).'">'.$tag->name.'</a> ';
		}
		 ?>
	 </div>

</div>


<?php


 if ( has_post_thumbnail() and $layout_option == 'b') {
	 $featured_image = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID),'tr-feature');
	 echo '<div class="featured-image" style="background-image:url('.$featured_image[0] .')"></div>';
 }

?>


<div class="content">
	<?php echo tr_social_share_buttons('article-top'); ?>
	<div class="left">
		<?php
	 	the_content();
		echo tr_social_share_buttons('article-bottom');

		// RFC 2460 - 09/11/17 updated by YR ?>
		<div style="margin-top:20px;"><iframe id="subscriptionIframe02" width="100%" height="" scrolling="no" class="yousuf" frameborder="0"></iframe></div>
		
		<?php
		if(get_post_meta(get_the_ID(), 'is_platinum', true) == false){	?>
		<div class="OUTBRAIN" data-src="<?php echo get_permalink(); ?>" data-widget-id="AR_1" data-ob-template="archant"></div>
		<script type="text/javascript" async="async" src="https://widgets.outbrain.com/outbrain.js"></script>	
		<?php } ?>	
	</div>

	<?php get_sidebar(); ?>

</div><!-- .content -->


<?php
} else echo '<p>no posts</p>';

get_footer();

# Removed visitor on post views - seems overkill? 10-08-18 CH
#$sql = "UPDATE ".$wpdb->postmeta." SET `meta_value` = (`meta_value`+ 1) WHERE `post_id` = '". $post->ID ."' AND `meta_key` = 'cb_visit_counter'";
#$wpdb->query($sql);

 ?>
<!-- RFC 2460 - 09/11/17 updated by YR -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/iframe-resizer/3.5.14/iframeResizer.min.js"></script>
<script>
var newsletterUrl = 'https://subscriptions.archant.co.uk/external-widget-bootstrap?origin=';
newsletterUrl += encodeURIComponent(window.location.protocol + "//" + window.location.hostname);
newsletterUrl += '&inArticle'
newsletterUrl += '&inArticleNewsletterFrequency=' + encodeURIComponent('weekly');
newsletterUrl += '&productName=' + encodeURIComponent('The Resident');
newsletterUrl += '&themeColor=' + encodeURIComponent('#d1ad54');
document.getElementById('subscriptionIframe02').onload = function() { 
    iFrameResize([{}], '#subscriptionIframe02'); 
};
document.getElementById('subscriptionIframe02').src = newsletterUrl;
</script>
