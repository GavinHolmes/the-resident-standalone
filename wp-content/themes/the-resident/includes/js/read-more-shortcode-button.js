(function() {
	tinymce.create('tinymce.plugins.readmore', {
		init : function(editor, url) {

			editor.addButton('readmore-shortcode', {
				title : 'Add Read More',
				cmd : 'read-more-shortcode',
				image: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEgAAABICAMAAABiM0N1AAABBVBMVEUAAABaWlpKSkpLS0tUVFRVVVVNTU1SUlJEREQzMzNAQEBEREQ/Pz8zMzM6Ojo+Pj46Ojo6Ojo9PT06Ojo8PDw5OTk5OTk6Ojo6Ojo5OTk5OTk3Nzc5OTk4ODg5OTk3Nzc4ODg3Nzc3Nzc2NjYyMjIzMzM2NjY2NjY2NjY1NTU1NTU1NTU2NjY1NTU1NTU1NTU0NDQ1NTU1NTU1NTU1NTU1NTU1NTU1NTUyMjIzMzM1NTU1NTUzMzM1NTU0NDQ1NTU0NDQ1NTUyMjIzMzM0NDQ0NDQ0NDQyMjIzMzM0NDQ0NDQ0NDQ0NDQ0NDQzMzM0NDQzMzMzMzMzMzMzMzMzMzMyMjIzMzMsPlY8AAAAVXRSTlMAAgQEBAQGCAoMDAwQFhoaHB4eICImKCgqLjA0NDY2Oj5CRFBUVF5gZGhscnJ0enx+foWHiYuNk5WVlZebn6OjpaWrq63D09XV2d3h4+fx8/X3+fv9IDyvAQAAAnhJREFUeAHtlvlS2zAQh5eWG1JaQstBuGMOWg5oAZujSZr4AMJR27vv/yiVzE5wLCUTVzMMf/iTUQya+UbaX1YDFBQUvDXLlfmZMRiYsZn5yjLo8EnwdOfWT7dXpqAnUyvbp3X37okEAegI6JX49vfeAigs7NVvY2IGEDHtq82P0OHD5lWb0mBPERIhkhhyTiZ6+PkFEsq/HpCQn86bIiqVJgB8FmHyiWyLGl8BvjUiRL1oolRKiRAdKRIcb+2ftbyQRQnRxUWEKUKvdba/dSzXfQAHMSsKUGCBZHRx9+aeUIHub3YXR0FioSDIiogcjt9KRXToURfeYSpIiwQ+gE2UFQUsSrFai4iJaqupBRYFiujlaCxKU67FJIhrZejG4mLbmBGpR2OuSXANWSxCQj6aLjVVZKPAVkXIIsQBRQ4KHJ0IWWS2I46fRWpqVXVHRHKZUVITy6YiZJE2/nzF5qMp8SOReWr2/xSbRdpeq6oiEqiiXr1mq72mitRia5vWz5+anywPXGyivqmR7mIziN+g1/pctTlEfa5aTs2s1zBvr3GN9Knl/B5xi2jjN7gh1RrlaBH1m21w1TqmTVtVm/bd3JD6pq3mF+lbxL082tmYG+lX7JG5jZ2jS5dFYll3sTGhVz9YH1eLPb5+UPdCYvrFnyZ0TypDrzsaqpy4IXbja3fkk0r7fOkltaXzNqloWqQ5O5wcDcWQYOeXv0TJhOKRPzwoqdHwbDMjIoxivYhJXtMiOccRYaZGOoin7g/K/n+brlHzjxzykS9Nnpst9xk7PLut5K88+JHvMACT3x8p4fHHJJjxuREhRrVPYM702to0FBQUvBP+AUvCurV7IVGDAAAAAElFTkSuQmCC'
				//image : url + '/../images/car.png'
			});

			editor.addCommand('read-more-shortcode', function() {

				var data = {
					title: 'Default Title',
					limit: 10
				}
				var checked = document.querySelectorAll('#categorychecklist input:checked');
				var cats = [];
				for(var i=0; i<checked.length; i++){
					cats.push(checked[i].value);
				}

				// Pass any post ids through to script in order to pre-populate
				var blocks = window.parent.tinyMCE.activeEditor.selection.getSelectedBlocks();
				var post_ids = '';

				if(blocks.length == 1){ // If there is one content block...

					// Find the first instance of readmore and populate the form
					readMoreShortcode = window.parent.wp.shortcode.next('readmore', blocks[0].innerHTML);
					if(readMoreShortcode != undefined){ // ... and it contains directions shortcode
						post_ids = readMoreShortcode.shortcode.get('post_ids');
					}
				}

				editor.windowManager.open({
					title: "Read More Options",
					url: ajaxurl
							+ '?action=tr_read_more_form&cats='
							+ cats.join(',')
							+ '&post_id='+document.getElementById('post_ID').value
							+ 'ts='+new Date().getTime()
							+ '&post_ids=' + post_ids,
					width: 550,
					height: 650,
					inline:1,
					data:data
				}, {
					arg1: 42,
					arg2: "Hello world"
				});

			});
		}
	});
	tinymce.PluginManager.add('readmore', tinymce.plugins.readmore);
})();
