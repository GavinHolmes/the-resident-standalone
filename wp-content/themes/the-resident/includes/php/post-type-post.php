<?php

add_action( 'init', function() {

	register_taxonomy(
		'the_author',
		'post',
		array(
			'label' => 'Author',
			'rewrite' => array( 'slug' => 'the_author' ),
			'show_admin_column' => true,
		)
	);

});

//remove default Author and Comments from the post list
add_filter('manage_posts_columns', function($columns) {
	unset($columns['author']);
	unset($columns['comments']);
	//var_dump($columns);
	return $columns;
}, 10, 1);

add_action('the_author_edit_form_fields', function($term) {	?>

	<tr class="form-field">
		<th><label>Set the image</label></th>
		<td><?php echo tr_render_the_author_image($term); ?></td>
	</tr>
<?php } );

add_action('the_author_add_form_fields',function($term) {
	echo tr_render_the_author_image($term);
});

function tr_render_the_author_image($term) {


	wp_enqueue_media();
	$meta = get_term_meta( $term->term_id );

	?>
	<script type="text/javascript">
		$j = jQuery.noConflict();

		var file_frame;
		var selected = [];

		function selectAFile(selectCallback, openCallback){
			file_frame = null; // remove old instance of media uploader
			selected_ids = [];

			// If the media frame already exists, reopen it.
			if ( file_frame ) {
				file_frame.open();
				return;
			}

			// Create the media frame.
			file_frame = wp.media.frames.file_frame = wp.media({
				title: 'Select an image',
				multiple: false
			});

			// Set custom callbacks
			file_frame.on( 'select', function(){ selectCallback(); });
			file_frame.on('open', function(){ openCallback() });

			file_frame.open(); // Finally, open the modal
		}

		function removeImageFile(){
			$j('#image-file-url').html('');
			$j('input[name="meta[tr-author-image-id]"]').val('');
		}

		function imageSelectCallback() {
			var selection = file_frame.state().get('selection');
			selection.map( function( attachment ) {
				attachment = attachment.toJSON();
				selected_ids.push(attachment.id);
				//$j('#tr-author-image-wrap').addClass('remove');
				$j('#image-file-url').empty().append('<a target="_blank" href="'+attachment.url+'"><img src="'+attachment.sizes.thumbnail.url+'"></a>');
			});
			$j('input[name="meta[tr-author-image-id]"]').val(selected_ids[0]);
		}

		function imageOpenCallback(){
			var selection = file_frame.state().get('selection');

			if($j('input[name="meta[tr-author-image-id]"]').length){
				attachment = wp.media.attachment($j('input[name="meta[tr-author-image-id]"]').val());
				attachment.fetch();
				selection.add( [ attachment ] );
			}
		}

	</script>

	<div id="tr-author-image-wrap">
		<p id="image-file-url">
			<?php if($meta['tr-author-image-id'][0]) {
				$img = wp_get_attachment_image_src($meta['tr-author-image-id'][0], 'thumbnail'); ?>
				<a target="_blank" href="<?php echo wp_get_attachment_url($meta['tr-author-image-id'][0]); ?>">
				<img src="<?php echo $img[0]; ?>">
			</a>
			</p>
			<?php } ?>
		</p>
		<button onclick="selectAFile(imageSelectCallback, imageOpenCallback);" type="button" class="button">Upload file</button>
		<button onclick="removeImageFile();" type="button" class="button remove">Remove file</button>
		<input type="hidden" name="meta[tr-author-image-id]" value="<?php echo $meta['tr-author-image-id'][0]; ?>">
		<br /><br />
	</div>

<?php }



function tr_the_auther_term_meta($term_id) {

	if ( isset($_POST['meta']['tr-author-image-id']) )
		update_term_meta($term_id, 'tr-author-image-id', $_POST['meta']['tr-author-image-id'] );

}
add_action( 'create_the_author', 'tr_the_auther_term_meta');
add_action( 'edit_the_author','tr_the_auther_term_meta' );

add_filter('manage_edit-the_author_columns', function($columns) {

	$new_columns['cb'] = $columns['cb'];
	$new_columns['name'] = $columns['name'];
	$new_columns['description'] = $columns['description'];
	$new_columns['image'] = 'Image';
	$new_columns['posts'] = $columns['posts'];
	return $new_columns;
} ,  5, 1);

add_filter('manage_the_author_custom_column', function($content,$column,$term_id){

	if ($column === 'image') {
		$t = get_term_meta( $term_id, 'tr-author-image-id', true );
		if ( !empty($t) ) {
			$thumbnail = wp_get_attachment_image_src( get_term_meta( $term_id, 'tr-author-image-id', true ) , 'thumbnail' );
			echo '<img style="width:50px; height:50px;" src="'.$thumbnail[0].'">';
		} else 	echo "not set";
	}

} , 10 , 3);








add_action( 'add_meta_boxes',function(){
	add_meta_box( 'post_options_meta_box','Post Options','tr_post_options_meta_box_render','post','side','high' );
});

function tr_post_options_meta_box_render() {

	global $post;
	$post_meta = get_post_meta($post->ID);

	?>

	<style>
		div.custom_meta {
			margin: 0 0 10px 0;
		}
		div.custom_meta select {
			width: 100%;
		}
	</style>
	<p>Page has been loaded <strong><?php echo isset($post_meta['cb_visit_counter'][0])? $post_meta['cb_visit_counter'][0]:'0';  ?></strong> times.</p>
	<input type="hidden" name="meta[cb_visit_counter]" value="<?php echo isset($post_meta['cb_visit_counter'][0])? $post_meta['cb_visit_counter'][0]:'0';  ?>" >

	<div class="custom_meta">
		<label>Is this Platinum: <select name="meta[is_platinum]">
			<option  value="0" <?php if($post_meta['is_platinum'][0] == 0) echo 'selected="selected"'; ?>>No</option>
			<option value="1" <?php if($post_meta['is_platinum'][0] == 1) echo 'selected="selected"'; ?>>Yes</option>
		</select></label>
	</div>

	<div class="custom_meta">
		<label>Is this a Ad Feature: <select name="meta[is_ad_feature]">
			<option  value="0" <?php if($post_meta['is_ad_feature'][0] == 0) echo 'selected="selected"'; ?>>No</option>
			<option value="1" <?php if($post_meta['is_ad_feature'][0] == 1) echo 'selected="selected"'; ?>>Yes</option>
		</select></label>
	</div>

	<div class="custom_meta">
		<label>Post layout: <select name="meta[layout_option]">
			<option value="a" <?php if($post_meta['layout_option'][0] == a) echo 'selected="selected"'; ?>>A: Deafult</option>
			<option value="b" <?php if($post_meta['layout_option'][0] == b) echo 'selected="selected"'; ?>>B: Featured image under title</option>
			<option value="c" <?php if($post_meta['layout_option'][0] == c) echo 'selected="selected"'; ?>>C: No Featured image</option>
		</select></label>
	</div>

	<?php

}

add_shortcode( 'dropcap', 'add_shortcode_dropcap');
function add_shortcode_dropcap($atts,$content = ""){
	return '<strong class="dropcap">'.$content.'</strong> ';
};

add_shortcode( 'divider', 'add_shortcode_divider');
function add_shortcode_divider($atts,$content = ""){
	return '<div class="divider'. (strlen($content) > 30 ? ' long':'') .'"><span>'.$content.'</span></div> ';
};

add_shortcode( 'alert', 'add_shortcode_alert');
function add_shortcode_alert($atts,$content = ""){
	return '<div class="alert_shorcode">'.$content.'</div> ';
};

add_shortcode( 'iframe', 'add_shortcode_iframe');
function add_shortcode_iframe( $atts, $content = null ) {
	$defaults = array(
		'src' => 'https://www.google.co.uk/',
		'width' => '100%',
		'height' => '480',
		'scrolling' => 'no',
		'class' => 'iframe-class',
		'frameborder' => '0'
	);

	foreach ( $defaults as $default => $value ) { // add defaults
		if ( ! @array_key_exists( $default, $atts ) ) $atts[$default] = $value;
	}

	$html = '';
	$html .= '<iframe';
	foreach( $atts as $attr => $value ) {
		if ( $value != '' ) $html .= ' ' . $attr . '="' . $value . '"';
		else $html .= ' ' . $attr;
	}
	$html .= '></iframe>'."\n";

	return $html;
};
