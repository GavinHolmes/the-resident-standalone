<?php
function tr_update_navigation_cache(){
	add_filter('walker_nav_menu_start_el', function( $item_output, $item, $depth, $args){

		// If this is a top-level menu item, add the drop down
		if($item->menu_item_parent == 0){

			// Fetch sub-categories
			$cat_posts = new WP_Query(array(
				'post_type' => 'post',
				'cat' => $item->object_id,
				'posts_per_page' => 1,
				'fields' => 'ids'
			));

			if(count($cat_posts->posts)){
				$item->has_drop_down = true; // Used to close divs later on
				$item_output .= '<div class="drop-down"><div class="sub-cats"><span class="more">MORE <span class="cat-name">'.strtoupper($item->title).'</span></span>' ;
			}
		}
		return $item_output;
	}, 10, 4);

	// Extends walker class in order to add closing divs (if necessary) at the end of each relevant element
	class TheResidentNavWalker extends Walker_Nav_Menu{
		public function end_el( &$output, $item, $depth = 0, $args = array() ) {
			if($item->has_drop_down) {

				$output .= '</div><!-- /sub-cats  -->'.PHP_EOL;

				// Fetch sub-categories
				$cat_posts = new WP_Query(array(
					'post_type' => 'post',
					'post_status' => 'publish',
					'cat' => $item->object_id,
					'posts_per_page' => 4
				));

				$i=0;
				$output .= '<div class="latest-posts">';
				while($cat_posts->have_posts()){
					$featured = ($i++ == 0); // first post is featured (has image)
					$cat_posts->the_post();
					$output .= '<div class="story '.($featured ? 'featured' : '').'">';
					$categories = get_the_category();
					$categories_html = '';
					if($featured){
						$output .= '<a href="'.get_permalink().'">'.get_the_post_thumbnail(get_the_ID(), 'tr-main').'</a>';
					} else {
						$cat_count = 0;
						$cat_limit = get_post_meta(get_the_ID(),'is_ad_feature',true) == 1 ? 1 : 2;
						foreach($categories as $category) {
							if($cat_count++ == $cat_limit) break;
							$categories_html .= '<a class="category" href="'.get_category_link($category->term_id).'">' . $category->name .'</a>';
						}
					}
					if(get_post_meta(get_the_ID(),'is_ad_feature',true) == 1) $output .= '<strong class="ad_feature">Ad Feature</strong> ';
					$output .= $categories_html;
					$output .= '<a href="'.get_permalink().'">';

					$output .= get_the_title().'</a></div>';
					if($featured) $output .= '<div class="right">';
				}
				wp_reset_postdata();
				$output .= '</div><!-- /right -->';
				$output .= '</div><!-- /latest-posts -->';
				$output .= '</div><!-- /drop-down  -->'.PHP_EOL;
			}
	    $output .= "</li>\n";
		}
	}

	// Return the html as variable
	$html = wp_nav_menu( array(
		'menu_id' => 'off-canvas-menu',
		'menu_class' => 'menu',
		'theme_location' => 'header-menu',
		'fallback_cb' => 'no_menu_set',
		'depth' => 0,
		'container_id' => 'menu_holder',
		'echo' => false,
		'walker' => new TheResidentNavWalker
	 ));

	 // Dump into a file
	 $upload_dir = wp_upload_dir(); // Get path to upload directory
	 $cache_path = $upload_dir['basedir'].'/navigation.html';
	 file_put_contents($cache_path, $html);
 }

// Define the ajax callback in order to call from URL
function tr_update_navigation_cache_callback() {
	tr_update_navigation_cache();
	wp_die();
}
// Need to define nopriv version as will probably called from cron job
add_action( 'wp_ajax_tr_update_navigation_cache', 'tr_update_navigation_cache_callback' );
add_action( 'wp_ajax_nopriv_tr_update_navigation_cache', 'tr_update_navigation_cache_callback' );

add_action('admin_menu', function(){

	add_submenu_page( 'themes.php', 'Update navigation', 'Update navigation', 'edit_posts', 'update-navigation', function(){
		?>
		<script>
	function updateNavigation(){
		var button = jQuery('#nav-update-button');
		button.text('Please wait');
		button.attr('disabled', 'disabled');
		jQuery.get({
			'url' : '/wp-admin/admin-ajax.php?action=tr_update_navigation_cache',
			'success' : function(){
					button.text('Update');
					button.removeAttr('disabled');
				}
			});
	}
		</script>
		<div id="wrap">
			<h1>Update navigation</h1>
			<button id="nav-update-button" class="button-primary" type="button" onclick="updateNavigation();">Update</button>
		</div>
		<?php
	});
});
