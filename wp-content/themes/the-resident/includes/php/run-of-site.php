<?php

$content_width = 940;
show_admin_bar(false);
add_theme_support( 'post-thumbnails' );
add_image_size( 'tr-small',  200,  150 , true ); // 4:3
add_image_size( 'tr-main',  600,  400 , true ); //  3:2
add_image_size( 'tr-large',  720,  620 , true ); // 36:31
add_image_size( 'tr-feature',  2160,  960 , true ); // 9:4

add_action('save_post', function(){
	global $post_id;
	if(wp_is_post_revision($post_id)) return;
	if(isset($_POST['meta']) && is_array($_POST['meta'])){
		foreach($_POST['meta'] as $field=>$value) update_post_meta($post_id, $field, $value);
	}
});

//Add in the Menu so wordpress knows this theme has two.
add_action( 'init', function(){
	register_nav_menu('header-menu', 'Header Menu');
	register_nav_menu('footer-menu', 'Footer Menu');
});
function no_menu_set() {
	echo '<p>No menu set</p>';
}

// Disable REST API
add_filter('rest_enabled', '_return_false');
add_filter('rest_jsonp_enabled', '_return_false');

add_action( 'wp_enqueue_scripts', function(){
	wp_enqueue_style( 'the-resident-style', get_stylesheet_uri() );
});

add_action( 'admin_init', function() {
	add_editor_style( get_stylesheet_directory_uri().'/includes/css/editor.css');
});


//Disable Emojis
add_action( 'init', function() {

	remove_action( 'admin_print_styles', 'print_emoji_styles' );
	remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
	remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
	remove_action( 'wp_print_styles', 'print_emoji_styles' );
	remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
	remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
	remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );

	add_filter( 'tiny_mce_plugins', 'bf_disable_emojicons_tinymce' );

});
function bf_disable_emojicons_tinymce( $plugins ) {
	if ( is_array( $plugins ) ) return array_diff( $plugins, array( 'wpemoji' ) );
	else return array();
}





// google analytics
add_action('wp_head', function(){
	if(is_user_logged_in()) return; ?>
	<script>
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

		ga('create', 'UA-50787464-1', 'auto');
		ga('set', 'anonymizeIp', true);
		ga('send', 'pageview');
	</script>
<?php });

add_action('wp_footer', function(){
	if(is_user_logged_in()) return;
?>

<script language="JavaScript" type="text/javascript">
	var s_account="archantswmag";
</script>
<script language="JavaScript" type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/includes/js/VisitorAPI.js"></script>
<script language="JavaScript" type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/includes/js/AppMeasurement.js"></script>
<script>
//initialize AppMeasurement
var s=s_gi(s_account)

/******** VISITOR ID SERVICE CONFIG - REQUIRES VisitorAPI.js ********/
s.visitor=Visitor.getInstance("2C000C3A53DA83BF0A490D4D@AdobeOrg")

/************************** CONFIG SECTION **************************/
/* You may add or alter any code config here. */
/* Link Tracking Config */
s.trackDownloadLinks=true
s.trackExternalLinks=true
s.trackInlineStats=true
s.linkDownloadFileTypes="exe,zip,wav,mp3,mov,mpg,avi,wmv,pdf,doc,docx,xls,xlsx,ppt,pptx"
s.linkInternalFilters="javascript:" //optional: add your internal domain here
s.linkLeaveQueryString=false
s.linkTrackVars="None"
s.linkTrackEvents="None"

/* uncomment below to use doPlugins */
/* s.usePlugins=true
function s_doPlugins(s) {

// use implementation plug-ins that are defined below
// in this section. For example, if you copied the append
// list plug-in code below, you could call:
// s.events=s.apl(s.events,"event1",",",1);

}
s.doPlugins=s_doPlugins */

/* WARNING: Changing any of the below variables will cause drastic
changes to how your visitor data is collected.  Changes should only be
made when instructed to do so by your account manager.*/
s.trackingServer="archant.d3.sc.omtrdc.net"
s.trackingServerSecure="archant.d3.sc.omtrdc.net"

/************************** PLUGINS SECTION *************************/

// copy and paste implementation plug-ins here - See "Implementation Plug-ins" @
// https://marketing.adobe.com/resources/help/en_US/sc/implement/#Implementation_Plugins
// Plug-ins can then be used in the s_doPlugins(s) function above

/****************************** MODULES *****************************/

// copy and paste implementation modules (Media, Integrate) here
// AppMeasurement_Module_Media.js - Media Module, included in AppMeasurement zip
// AppMeasurement_Module_Integrate.js - Integrate Module, included in AppMeasurement zip

/* ============== DO NOT ALTER ANYTHING BELOW THIS LINE ! =============== */
</script>
<script language="JavaScript" type="text/javascript"><!--

<?php

global $tr_page_title, $post, $wp_query;
if(is_front_page()) {
	$channel = $tr_page_title;
	$prop2 = 'http'.($_SERVER['HTTPS'] ? 's' : '').'://'.$_SERVER['HTTP_HOST'].'/?p='.$post->ID;
	$pageType = 'Home';
} elseif (is_page()) {
	$channel = $tr_page_title;
	$pageType = 'Static Page';
	$prop2 = 'http'.($_SERVER['HTTPS'] ? 's' : '').'://'.$_SERVER['HTTP_HOST'].'/?p='.$post->ID;
} elseif (is_single()) {
	$categories = get_the_category();
	$prop2 = 'http'.($_SERVER['HTTPS'] ? 's' : '').'://'.$_SERVER['HTTP_HOST'].'/?p='.$post->ID;
	$channel = $categories[0]->name;
	$pageType = 'Article';
	$cat_names = array();
	$prop3 = get_the_author();

	foreach($categories as $cat) $cat_names[]= $cat->name;
	$prop1 = implode('|', $cat_names);
} elseif (is_category()) {
	$categories = get_the_category();
	$channel = $categories[0]->name;
	$prop2 = get_category_link($wp_query->get_queried_object_id());
	$pageType =  'Category';
} elseif (is_tag()) {
	$channel = $tr_page_title;
	$pageType = 'Tag';
	$prop2 = get_tag_link($wp_query->get_queried_object_id());
}
/*elseif (is_month()) {
	list($month, $year) = split(' ', the_date('F Y', '', '', false));
	$channel = $pageType = 'Month Archive';
	$prop2 = get_month_link($year, $month);
} */
else {
	$pageName = $channel = $tr_page_title;
	$pageType = 'Other';
}

$hier1 = str_replace('/', '||', preg_replace('/(^\/)|(\/$)/', '', 'home' . $_SERVER['REQUEST_URI']));

// Using json_encode to escape javascript strings
echo "s.pageName = ".json_encode($tr_page_title)."; \n";
echo "s.channel = ".json_encode($channel)."; \n";
echo "s.pageType = '".$pageType."'; \n";
if($prop1) echo "s.prop1 = ".json_encode($prop1)."; \n"; // editorial category, determined by content uploader to reflect type of page’s content focus [e.g. “Tourism:Housing”]
if($prop2) echo "s.prop2 = '".$prop2."'; \n";  // page URL, automatically generated – note it is not the full URL, just the site domain and the page ID; this is important as this allows us to track traffic to a single page if the pageName subsequently changes [e.g. http://www.edp24.co.uk/1.4251087]
if($prop3) echo "s.prop3 = ".json_encode($prop3)."; \n"; // author, i.e. content uploader’s email address [e.g. Kieran.lynch@archant.co.uk]
if($prop1) echo "s.prop4 = ".json_encode($prop1)."; \n"; // content string, reflects the page’s position in the site’s content hierarchy [e.g. “Category.General.Tourism.NewsSocial”]
if($prop5) echo "s.prop5 = ''; \n"; // keyword, as determined by the content uploader I believe [e.g. “Great Yarmouth”]


echo "s.hier1= ".json_encode($hier1).';'; // should reflect the page’s position in the sitemap, with site section first then followed by the relevant sub-section(s) [e.g. “Business|Tourism”]
?>

s.linkInternalFilters="<?php echo $_SERVER['HTTP_HOST']; ?>";
s.campaign=""; s.state=""; s.zip=""; s.events=""; s.products=""; s.purchaseID=""; s.eVar1=""; s.eVar2=""; s.eVar3=""; s.eVar4=""; s.eVar5="";

var s_code=s.t();if(s_code)document.write(s_code)//-->
</script>
<script language="JavaScript" type="text/javascript">
<!--
if(navigator.appVersion.indexOf('MSIE')>=0)document.write(unescape('%3C')+'\!-'+'-')
//-->
</script>

<?php });

// adds class to body tag if flag within post meta is set to 1
function check_is_platinum($post_id) {
	if(get_post_meta($post_id, 'is_platinum', true)) add_filter( 'body_class', function( $classes ) {
		return array_merge( $classes, array( 'is_platinum' ) );
	} );
}

add_action( 'add_meta_boxes',function(){
	add_meta_box( 'tr_seo_meta_box','SEO Options','tr_seo_meta_box_render',array('page','post'),'normal','high' );
});
function tr_seo_meta_box_render() {

	global $post;
	$post_meta = get_post_meta($post->ID);

	?>

	<style>
		div.custom_meta {
			margin: 0 0 10px 0;
		}
		div.custom_meta input {
			width: calc(100% - 4px);
		}
	</style>

	<div class="custom_meta">
		<label for="meta_su_title">Page Title</label><br />
		<input type="text" id="meta_su_title" name="meta[_su_title]" value="<?php echo $post_meta['_su_title'][0]; ?>">
	</div>

	<div class="custom_meta">
		<label for="meta_su_description">Meta Description</label><br />
		<input type="text" id="meta_su_description" name="meta[_su_description]" value="<?php echo $post_meta['_su_description'][0]; ?>">
	</div>

	<?php

}

function pagination($wp_query=null, $max_num_pages = false, $query_string = ''){
	if($wp_query === null) global $wp_query;

	if($wp_query->found_posts <= $wp_query->query_vars['posts_per_page']) return '';

	if(count($wp_query->posts) === 0) return '';

	if(isset($_GET['paged'])) $paged = $_GET['paged'] ? $_GET['paged'] : 1;
	else $paged = get_query_var('paged') ? get_query_var('paged') : 1;

	return '<div id="pagination">' . paginate_links( array(
		'base' => str_replace( 999999999, '%#%', esc_url( get_pagenum_link( 999999999 ) ) ), // ugly hack to make pagination work when query string is present, eg search
		'format' => '?paged=%#%',
		'current' => max( 1, $paged),
		'total' => $max_num_pages?$max_num_pages:$wp_query->max_num_pages,
		'prev_text'    =>'<span>Previous</span>',
		'next_text'    => '<span>Next</span>',
		'before_page_number' => '<span>',
		'after_page_number' => '</span>'
	)) . "</div>";

}

function tr_social_share_buttons($classes){
	global $featured_image;
	$url = urlencode('http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']);
	return '
	<ul class="share-buttons '.$classes.'">
			<li><a class="facebook" onclick="var win = window.open(this.href, \'share_window\', \'menubar=no,location=yes,resizable=yes,scrollbars=no,status=no,width=600,height=500\'); win.focus(); return false;" href="https://www.facebook.com/sharer.php?u='.$url.'" target="_blank">Facebook</a></li>
			<li><a class="twitter" onclick="var win = window.open(this.href, \'share_window\', \'menubar=no,location=yes,resizable=yes,scrollbars=no,status=no,width=600,height=500\'); win.focus(); return false;" href="https://twitter.com/intent/tweet?text='.urlencode(get_the_title()).'&amp;url='.$url.'" target="_blank">Twitter</a></li>
			<li><a class="pinterest" onclick="var win = window.open(this.href, \'share_window\', \'menubar=no,location=yes,resizable=yes,scrollbars=no,status=no,width=730,height=650\'); win.focus(); return false;" href="https://pinterest.com/pin/create/button/?url='.$url.'&media='.urlencode($featured_image[0]).'" target="_blank">Pinterest</a></li>
		</ul>';

	}

// Enqueue footer scripts
add_action('wp_enqueue_scripts', function() {
    wp_enqueue_script( 'footer-scripts', get_template_directory_uri() . '/includes/js/footer.js', array(), '1.0.0', true );
});

add_action( 'admin_enqueue_scripts', function () {
  wp_register_style( 'admin_css', get_template_directory_uri() . '/includes/css/admin.css', false, '1.0.0' );
  wp_enqueue_style( 'admin_css');
});

if( !isset($_COOKIE['accept_cookies']) ) {

function add_cookie_notice() { ?>
<!-- Removed Cookie Popup GDPR 23/05/18 -->
<script type="text/javascript">


</script>

<?php }

	add_action('wp_footer', 'add_cookie_notice');

}

function tr_render_lotame_script(){ ?>
	<!-- Lotame -->
	<script src="https://tags.crwdcntrl.net/c/4781/cc.js?ns=_cc4781" id="LOTCC_4781"></script>
	<script>
	   _cc4781.bcp();
	</script>
	<script type="text/javascript">
	   var cc_client_id= 7659;
	   document.write('<scr'+'ipt type="text/javascript" src="https://ad.crwdcntrl.net/5/c='+cc_client_id.toString()+'/pe=y/callback=processauds"></scr'+'ipt>');
	   var adtechCCvals = "";
	   function processauds(adtechccauds) {
		   if (typeof(adtechccauds) != 'undefined'){
			   for (var cci = 0; cci < adtechccauds.Profile.Audiences.Audience.length;cci++){
			   if (cci > 0) adtechCCvals += ":";
			   adtechCCvals +=adtechccauds.Profile.Audiences.Audience[cci].abbr;
			   }
		   }
	   }
	</script>
	<!-- End Lotame -->
<?php }
add_action('wp_head', 'tr_render_lotame_script');

function tr_render_quantcast_script(){
	?>
	<!-- Quantcast Tag -->
	<script>
	qcdata = {} || qcdata;
	(function(){
	var elem = document.createElement('script');
	elem.src = (document.location.protocol == "https:" ? "https://secure" : "http://pixel") + ".quantserve.com/aquant.js?a=p-YX7x3E8QMpsVE";
	elem.async = true;
	elem.type = "text/javascript";
	var scpt = document.getElementsByTagName('script')[0];
	scpt.parentNode.insertBefore(elem,scpt);
	}());

	var qcdata = {qacct: 'p-YX7x3E8QMpsVE',
	uid: 'USER-ID-HERE'};
	</script>

	<noscript>
	<img src="//pixel.quantserve.com/pixel/p-YX7x3E8QMpsVE.gif?labels=" style="display: none;" border="0" height="1" width="1" alt="Quantcast"/>
	</noscript>
	<!-- End Quantcast Tag -->
	<?php
}
add_action('wp_head', 'tr_render_quantcast_script');

function tr_add_post_thumbnail_to_rss() {
  global $post;
  if(has_post_thumbnail($post->ID)):
              $img = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'tr-main');
              echo("<image>{$img[0]}</image>");
  endif;
}
add_action('rss2_item', 'tr_add_post_thumbnail_to_rss');

add_shortcode('resident_newsletter', 'resident_newsletter_shortcode');

function resident_newsletter_shortcode(){
	ob_start();
	?>
	<div class="widget html ">
		<div>
			<iframe id="subscriptionIframe" width="100%" height="640" scrolling="no" frameborder="0"></iframe>
		</div>

		<script src="https://cdnjs.cloudflare.com/ajax/libs/iframe-resizer/3.5.14/iframeResizer.min.js"></script>
		<script>
		var newsletterUrl = 'https://subscriptions.archant.co.uk/external-widget-bootstrap?origin=';
		newsletterUrl += encodeURIComponent(window.location.protocol + "//" + window.location.hostname);
		newsletterUrl += '&inArticleNewsletterFrequency=' + encodeURIComponent('weekly');
		newsletterUrl += '&productName=' + encodeURIComponent('The Resident');
		newsletterUrl += '&themeStyleSheet=' + encodeURIComponent('/wp-content/themes/the-resident/includes/css/newsletter.css');
		document.getElementById('subscriptionIframe').onload = function() {
			iFrameResize([{}], '#subscriptionIframe');
		};
		document.getElementById('subscriptionIframe').src = newsletterUrl;
		</script>
	</div>
	<?php
	$content = ob_get_contents();
	ob_end_clean();
	return $content;
}
