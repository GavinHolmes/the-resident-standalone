<?php

add_action( 'add_meta_boxes',function(){
	add_meta_box( 'page_options_meta_box','Post Options','tr_page_options_meta_box_render','page','side','high' );
});

function tr_page_options_meta_box_render() {

	global $post;
	$post_meta = get_post_meta($post->ID);

	?>

	<style>
		div.custom_meta {
			margin: 0 0 10px 0;
		}
		div.custom_meta select {
			width: 100%;
		}
	</style>

	<div class="custom_meta">
		<label>Is this Platinum: <select name="meta[is_platinum]">
			<option  value="0" <?php if($post_meta['is_platinum'][0] == 0) echo 'selected="selected"'; ?>>No</option>
			<option value="1" <?php if($post_meta['is_platinum'][0] == 1) echo 'selected="selected"'; ?>>Yes</option>
		</select></label>
	</div>

	<?php

}
