<?php

add_action( 'widgets_init', function() {
	register_sidebar(array( 'description' => 'Shown on the Front Page', 'name' => 'Front Page', 'id' => 'front-page' ));
	register_sidebar(array( 'description' => 'Shown in the sidebar', 'name' => 'Sidebar', 'id' => 'sidebar' ));
	register_sidebar(array( 'description' => 'Shown in the header', 'name' => 'Header', 'id' => 'header' ));
});

add_action( 'widgets_init', function() {

	unregister_widget('WP_Widget_Pages');
	unregister_widget('WP_Widget_Calendar');
	unregister_widget('WP_Widget_Archives');
	unregister_widget('WP_Widget_Links');
	unregister_widget('WP_Widget_Meta');
	unregister_widget('WP_Widget_Search');
	unregister_widget('WP_Widget_Text');
	unregister_widget('WP_Widget_Categories');
	unregister_widget('WP_Widget_Recent_Posts');
	unregister_widget('WP_Widget_Recent_Comments');
	unregister_widget('WP_Widget_RSS');
	unregister_widget('WP_Widget_Tag_Cloud');
	unregister_widget('WP_Nav_Menu_Widget');

	register_widget('tr_search_form');
	register_widget('tr_article_list');
	register_widget('html');
	register_widget('six_article_intro');
	register_widget('large_article_list');
	register_widget('small_article_list');
	register_widget('platinum_article_list');
	register_widget('sliding_article_list');
	register_widget('render_adtech_advert');
	register_widget('homes_24');

});

class tr_search_form extends WP_Widget {

	function __construct() {
		parent::__construct( 'tr_search_form', 'Search Bar',
			array( 'description' =>"Simple search bar.")
		);
	}

	public function widget($args, $instance) {
		if(is_search() and $instance['show'] != true) return; ?>

		<form role="search" method="get" id="searchform" class="searchform widget" action="/">
			<div>
				<input type="text" value="<?php echo get_search_query(); ?>" name="s" id="s" placeholder="Search Website" />
				<input type="submit" id="searchsubmit" value="Search" />
			</div>
		</form>

	<?php }
	public function form($instance) { ?>

		<p>There are no setting for this widget</p>

	<?php
	}
}

class homes_24 extends WP_Widget {

	function __construct() {
		parent::__construct( 'homes_24', 'Homes24',
			array( 'description' =>"Adds the Homes24 box to your site.")
		);
	}

	public function widget($args, $instance) { ?>

		<style type="text/css">
			.zoopla img {
				border: 0
			}
			.zoopla {
				border: 1px solid #d0d0d0;
				font: 12px sans-serif;
				background: #fff url("https://theresident.wpms.greatbritishlife.co.uk/wp-content/uploads/sites/10/2015/10/homes24_widget_bg.png") repeat-x bottom center;
				padding: 20px;
				position: relative;
			}
			.zoopla_header_main {
				background: url("https://theresident.wpms.greatbritishlife.co.uk/wp-content/uploads/sites/10/2015/11/theResident.jpg") no-repeat;
				font-size: 12px;
				font-weight: bold;
				line-height: 40px;
				margin: 10px 0 10px 0;
				padding: 0 0 0 105px;
				text-align: right
			}
			.zoopla_separate {
				margin: 0 0 10px 0
			}
			.zoopla_hint {
				color: #999;
				font-size: 11px
			}
			.zoopla label {
				display: inline;
				font-weight: bold
			}
			.zoopla_location {
				width: 98%
			}
			select {
				width: 100%
			}
			.zoopla_button {
				border: 1px solid #e84d19;
				border-color: #f1231a;
				background: #f1291e;
				background-image: linear-gradient(bottom, rgb(241, 41, 30) 1%, rgb(248, 92, 87) 51%, rgb(255, 180, 176) 76%);
				background-image: -o-linear-gradient(bottom, rgb(241, 41, 30) 1%, rgb(248, 92, 87) 51%, rgb(255, 180, 176) 76%);
				background-image: -moz-linear-gradient(bottom, rgb(241, 41, 30) 1%, rgb(248, 92, 87) 51%, rgb(255, 180, 176) 76%);
				background-image: -webkit-linear-gradient(bottom, rgb(241, 41, 30) 1%, rgb(248, 92, 87) 51%, rgb(255, 180, 176) 76%);
				background-image: -ms-linear-gradient(bottom, rgb(241, 41, 30) 1%, rgb(248, 92, 87) 51%, rgb(255, 180, 176) 76%);
				background-image: -webkit-gradient( linear, left bottom, left top, color-stop(0.01, rgb(241, 41, 30)), color-stop(0.51, rgb(248, 92, 87)), color-stop(0.76, rgb(255, 180, 176)));
				border-radius: 5px;
				color: #fff;
				font-size: 1.2em;
				font-weight: bold;
				padding: .3em .6em;
				text-shadow: 0 1px #f54039
			}
			.zoopla_button:hover {
				cursor: pointer
			}
			.zoopla_split_2l {
				width: 49%;
				float: left
			}
			.zoopla_split_2r {
				width: 49%;
				float: right
			}
			.zoopla_split_inline {
				display: block;
				float: left;
				padding-right: 1em
			}
			.zoopla_small_input {
				width: 8em
			}
			.zoopla_powered_by {
				position: absolute;
				top: 10px;
				right: 20px
			}
			.zoopla .clearfix:after {
				clear: both;
				content: ".";
				display: block;
				font-size: 0;
				height: 0;
				visibility: hidden
			}
			* html .zoopla .clearfix {
				height: 1px
			}
			.zoopla .clearfix {
				display: block
			}
		</style>
		<!-- BEGIN - ZOOPLA SEARCH WIDGET -->
		<div class="zoopla" style="height:250px;">
			<p class="zoopla_header_main" style="font-size:12px;">Property Search</p>
			<form name="zoopla_search" action="https://www.homes24.co.uk/search/" method="post">
				<div class="zoopla_separate">
					<div class="clearfix">
						<div class="zoopla_split_inline">
							<label for="zoopla_search_sale">
								<input value="for-sale" id="zoopla_search_sale" name="section" onclick="zoopla_sale_function();" type="radio" checked="checked"> For sale</label>
						</div>
						<div class="zoopla_split_inline">
							<label for="zoopla_search_rent">
								<input value="to-rent" id="zoopla_search_rent" name="section" onclick="zoopla_rent_function();" type="radio"> To rent</label>
						</div>
						<div class="zoopla_split_inline">
							<label for="zoopla_search_values">
								<input value="home-values" id="zoopla_search_values" name="section" onclick="zoopla_values_prices_function();" type="radio"> Home values</label>
						</div>
					</div>
				</div>
				<div class="zoopla_separate">
					<label for="zoopla_search_location" class="zoopla_block">Location</label>
					<input id="zoopla_search_location" class="zoopla_location" name="q" onclick="zoopla_location_function();" onkeydown="zoopla_location_function();">
					<br><span id="zoopla_search_example" class="zoopla_hint">e.g. Oxford or NW3</span> </div>
				<div id="zoopla_search_attributes" class="zoopla_separate">
					<div id="zoopla_search_toggle_sale_price" class="zoopla_separate">
						<div class="zoopla_split_inline zoopla_small_input">
							<label for="zoopla_search_sale_price_min">Min price</label>
							<select name="price_min" id="zoopla_search_sale_price_min">
								<option value="" selected="selected">No min</option>
								<option value="10000">&pound;10,000</option>
								<option value="20000">&pound;20,000</option>
								<option value="30000">&pound;30,000</option>
								<option value="40000">&pound;40,000</option>
								<option value="50000">&pound;50,000</option>
								<option value="60000">&pound;60,000</option>
								<option value="70000">&pound;70,000</option>
								<option value="80000">&pound;80,000</option>
								<option value="90000">&pound;90,000</option>
								<option value="100000">&pound;100,000</option>
								<option value="125000">&pound;125,000</option>
								<option value="150000">&pound;150,000</option>
								<option value="175000">&pound;175,000</option>
								<option value="200000">&pound;200,000</option>
								<option value="225000">&pound;225,000</option>
								<option value="250000">&pound;250,000</option>
								<option value="300000">&pound;300,000</option>
								<option value="350000">&pound;350,000</option>
								<option value="400000">&pound;400,000</option>
								<option value="450000">&pound;450,000</option>
								<option value="500000">&pound;500,000</option>
								<option value="600000">&pound;600,000</option>
								<option value="700000">&pound;700,000</option>
								<option value="800000">&pound;800,000</option>
								<option value="900000">&pound;900,000</option>
								<option value="1000000">&pound;1,000,000</option>
								<option value="1250000">&pound;1,250,000</option>
								<option value="1500000">&pound;1,500,000</option>
								<option value="2000000">&pound;2,000,000</option>
								<option value="2500000">&pound;2,500,000</option>
								<option value="3000000">&pound;3,000,000</option>
								<option value="4000000">&pound;4,000,000</option>
								<option value="5000000">&pound;5,000,000+</option>
								<option value="">No min</option>
							</select>
						</div>
						<div class="zoopla_split_inline zoopla_small_input">
							<label for="zoopla_search_sale_price_max">Max price</label>
							<select name="price_max" id="zoopla_search_sale_price_max">
								<option value="" selected="selected">No max</option>
								<option value="10000">&pound;10,000</option>
								<option value="20000">&pound;20,000</option>
								<option value="30000">&pound;30,000</option>
								<option value="40000">&pound;40,000</option>
								<option value="50000">&pound;50,000</option>
								<option value="60000">&pound;60,000</option>
								<option value="70000">&pound;70,000</option>
								<option value="80000">&pound;80,000</option>
								<option value="90000">&pound;90,000</option>
								<option value="100000">&pound;100,000</option>
								<option value="125000">&pound;125,000</option>
								<option value="150000">&pound;150,000</option>
								<option value="175000">&pound;175,000</option>
								<option value="200000">&pound;200,000</option>
								<option value="225000">&pound;225,000</option>
								<option value="250000">&pound;250,000</option>
								<option value="300000">&pound;300,000</option>
								<option value="350000">&pound;350,000</option>
								<option value="400000">&pound;400,000</option>
								<option value="450000">&pound;450,000</option>
								<option value="500000">&pound;500,000</option>
								<option value="600000">&pound;600,000</option>
								<option value="700000">&pound;700,000</option>
								<option value="800000">&pound;800,000</option>
								<option value="900000">&pound;900,000</option>
								<option value="1000000">&pound;1,000,000</option>
								<option value="1250000">&pound;1,250,000</option>
								<option value="1500000">&pound;1,500,000</option>
								<option value="2000000">&pound;2,000,000</option>
								<option value="2500000">&pound;2,500,000</option>
								<option value="3000000">&pound;3,000,000</option>
								<option value="4000000">&pound;4,000,000</option>
								<option value="5000000">&pound;5,000,000+</option>
								<option value="">No max</option>
							</select>
						</div>
					</div>
					<div id="zoopla_search_toggle_rent_price" class="zoopla_separate">
						<div class="zoopla_split_inline zoopla_small_input">
							<label for="zoopla_search_rent_price_min">Min price</label>
							<select name="price_min" id="zoopla_search_rent_price_min" disabled="disabled">
								<option value="">No min</option>
								<option value="25">&pound;25 pm</option>
								<option value="50">&pound;50 pm</option>
								<option value="75">&pound;75 pm</option>
								<option value="100">&pound;100 pm</option>
								<option value="125">&pound;125 pm</option>
								<option value="150">&pound;150 pm</option>
								<option value="175">&pound;175 pm</option>
								<option value="200">&pound;200 pm</option>
								<option value="225">&pound;225 pm</option>
								<option value="250">&pound;250 pm</option>
								<option value="275">&pound;275 pm</option>
								<option value="300">&pound;300 pm</option>
								<option value="325">&pound;325 pm</option>
								<option value="350">&pound;350 pm</option>
								<option value="375">&pound;375 pm</option>
								<option value="400">&pound;400 pm</option>
								<option value="425">&pound;425 pm</option>
								<option value="450">&pound;450 pm</option>
								<option value="475">&pound;475 pm</option>
								<option value="500">&pound;500 pm</option>
								<option value="550">&pound;550 pm</option>
								<option value="600">&pound;600 pm</option>
								<option value="650">&pound;650 pm</option>
								<option value="700">&pound;700 pm</option>
								<option value="750">&pound;750 pm</option>
								<option value="800">&pound;800 pm</option>
								<option value="850">&pound;850 pm</option>
								<option value="900">&pound;900 pm</option>
								<option value="950">&pound;950 pm</option>
								<option value="1000">&pound;1,000 pm</option>
								<option value="1250">&pound;1,250 pm</option>
								<option value="1500">&pound;1,500 pm</option>
								<option value="1750">&pound;1,550 pm</option>
								<option value="2000">&pound;2,000+ pm</option>
								<option value="">No min</option>
							</select>
						</div>
						<div class="zoopla_split_inline zoopla_small_input">
							<label for="zoopla_search_rent_price_max">Max price</label>
							<select name="price_max" id="zoopla_search_rent_price_max" disabled="disabled">
								<option value="">No max</option>
								<option value="25">&pound;25 pm</option>
								<option value="50">&pound;50 pm</option>
								<option value="75">&pound;75 pm</option>
								<option value="100">&pound;100 pm</option>
								<option value="125">&pound;125 pm</option>
								<option value="150">&pound;150 pm</option>
								<option value="175">&pound;175 pm</option>
								<option value="200">&pound;200 pm</option>
								<option value="225">&pound;225 pm</option>
								<option value="250">&pound;250 pm</option>
								<option value="275">&pound;275 pm</option>
								<option value="300">&pound;300 pm</option>
								<option value="325">&pound;325 pm</option>
								<option value="350">&pound;350 pm</option>
								<option value="375">&pound;375 pm</option>
								<option value="400">&pound;400 pm</option>
								<option value="425">&pound;425 pm</option>
								<option value="450">&pound;450 pm</option>
								<option value="475">&pound;475 pm</option>
								<option value="500">&pound;500 pm</option>
								<option value="550">&pound;550 pm</option>
								<option value="600">&pound;600 pm</option>
								<option value="650">&pound;650 pm</option>
								<option value="700">&pound;700 pm</option>
								<option value="750">&pound;750 pm</option>
								<option value="800">&pound;800 pm</option>
								<option value="850">&pound;850 pm</option>
								<option value="900">&pound;900 pm</option>
								<option value="950">&pound;950 pm</option>
								<option value="1000">&pound;1,000 pm</option>
								<option value="1250">&pound;1,250 pm</option>
								<option value="1500">&pound;1,500 pm</option>
								<option value="1750">&pound;1,550 pm</option>
								<option value="2000">&pound;2,000+ pm</option>
								<option value="">No max</option>
							</select>
						</div>
					</div>
					<div class="zoopla_separate" id="zoopla_search_toggle_type_beds">
						<div class="zoopla_split_inline zoopla_small_input">
							<label for="zoopla_search_property_type">Type</label>
							<select name="property_type" id="zoopla_search_property_type">
								<option value="" selected="selected">Show all</option>
								<option value="houses">Houses</option>
								<option value="flats">Flats</option>
							</select>
						</div>
						<div class="zoopla_split_inline zoopla_small_input">
							<label for="zoopla_search_beds_min">Beds</label>
							<select name="beds_min" id="zoopla_search_beds_min">
								<option value="" selected="selected">No min</option>
								<option value="1">1+</option>
								<option value="2">2+</option>
								<option value="3">3+</option>
								<option value="4">4+</option>
								<option value="5">5+</option>
								<option value="">No min</option>
							</select>
						</div>
					</div>
				</div>
				<input id="zoopla_price_frequency" name="zoopla_price_frequency" value="per_week" type="hidden" disabled="disabled">
				<input name="utm_source" value="homes24" type="hidden">
				<input name="utm_medium" value="anglia_searchwidget" type="hidden">
				<div style="float:right;margin-top:-25px">
					<input type="submit" value="Search" id="zoopla_search_button" class="zoopla_button"> </div>
			</form>
			<a class="zoopla_powered_by" href="https://www.zoopla.co.uk/?utm_source=homes24&amp;utm_medium=anglia_searchwidget"><img src="https://theresident.wpms.greatbritishlife.co.uk/wp-content/uploads/sites/10/2015/10/homes24_widget_main_zoopla.png" height="18" width="111" alt="Powered by Zoopla"> </a>
		</div>
		<script type="text/javascript">

		if(typeof zoopla_hint_1=='undefined'){var zoopla_hint_1='Enter place name or area';var zoopla_hint_2='Enter address, street, postcode or area';var zoopla_example_1='e.g. Oxford or NW3';var zoopla_example_2='e.g. Acacia Avenue or TW19 5NW';}
		var zoopla_search_location=document.getElementById('zoopla_search_location'),zoopla_search_example=document.getElementById('zoopla_search_example'),zoopla_search_toggle_sale_price=document.getElementById('zoopla_search_toggle_sale_price'),zoopla_search_toggle_rent_price=document.getElementById('zoopla_search_toggle_rent_price'),zoopla_search_sale_price_min=document.getElementById('zoopla_search_sale_price_min'),zoopla_search_sale_price_max=document.getElementById('zoopla_search_sale_price_max'),zoopla_search_rent_price_min=document.getElementById('zoopla_search_rent_price_min'),zoopla_search_rent_price_max=document.getElementById('zoopla_search_rent_price_max'),zoopla_search_beds_min=document.getElementById('zoopla_search_beds_min'),zoopla_search_property_type=document.getElementById('zoopla_search_property_type'),zoopla_search_attributes=document.getElementById('zoopla_search_attributes'),zoopla_search_rent=document.getElementById('zoopla_search_rent'),zoopla_search_values=document.getElementById('zoopla_search_values'),zoopla_search_rent_price_frequency=document.getElementById('zoopla_price_frequency');function zoopla_change_hint(hint,example){if((zoopla_search_location.value===zoopla_hint_1)||(zoopla_search_location.value===zoopla_hint_2)||(zoopla_search_location.value==='')){zoopla_search_location.value=hint;zoopla_search_location.style.color='#999999';zoopla_search_example.innerHTML=example;}}
		function zoopla_location_function(){if((zoopla_search_location.value===zoopla_hint_1)||(zoopla_search_location.value===zoopla_hint_2)){zoopla_search_location.value='';zoopla_search_location.style.color='#000000';}}
		function zoopla_sale_function(){zoopla_search_toggle_sale_price.style.display='block';if(zoopla_search_toggle_rent_price!==null)zoopla_search_toggle_rent_price.style.display='none';zoopla_search_sale_price_min.disabled='';zoopla_search_sale_price_max.disabled='';if(zoopla_search_toggle_rent_price!==null)zoopla_search_rent_price_min.disabled='true';if(zoopla_search_toggle_rent_price!==null)zoopla_search_rent_price_max.disabled='true';zoopla_search_beds_min.disabled='';zoopla_search_property_type.disabled='';zoopla_search_attributes.style.display='block';zoopla_change_hint(zoopla_hint_1,zoopla_example_1);}
		function zoopla_rent_function(){zoopla_search_toggle_sale_price.style.display='none';zoopla_search_toggle_rent_price.style.display='block';zoopla_search_sale_price_min.disabled='true';zoopla_search_sale_price_max.disabled='true';zoopla_search_rent_price_min.disabled='';zoopla_search_rent_price_max.disabled='';zoopla_search_rent_price_frequency.disabled='';zoopla_search_beds_min.disabled='';zoopla_search_property_type.disabled='';zoopla_search_attributes.style.display='block';zoopla_change_hint(zoopla_hint_1,zoopla_example_1);}
		function zoopla_values_prices_function(){zoopla_search_attributes.style.display='none';zoopla_search_sale_price_min.disabled='true';zoopla_search_sale_price_max.disabled='true';zoopla_search_rent_price_min.disabled='true';zoopla_search_rent_price_max.disabled='true';zoopla_search_rent_price_frequency.disabled='true';zoopla_search_beds_min.disabled='true';zoopla_search_property_type.disabled='true';zoopla_change_hint(zoopla_hint_2,zoopla_example_2);}
		if(zoopla_search_values&&zoopla_search_values.checked===true){zoopla_values_prices_function();}else if(zoopla_search_rent&&zoopla_search_rent.checked===true){zoopla_rent_function();}else{zoopla_sale_function();}

		</script>

	<?php }
	public function form($instance) { ?>

		<p>No setting required.</p>

	<?php
	}
}

class tr_article_list extends WP_Widget {

	function __construct() {
		parent::__construct( 'tr_article_list', 'Article List',
			array( 'description' =>"Add a Article List to your site.")
		);
	}

	public function widget($arges, $instance) {

		global $post, $cats_for_widget, $post_ids_for_widget;
		$oldpost = $post;
		$is_ad_feature = false;

		//var_dump(is_single());
		//else var_dump($post);

		//var_dump($instance);

		if($instance['type'] == 'tranding') {
			$instance['title'] = '<h2 class="title">Trending</h2>';
			$article_list = new WP_Query(array(
				'posts_per_page' => 3,
				'orderby' => 'meta_value',
				'meta_key' => 'cb_visit_counter',
				'date_query' => array(
					array(
						'after'  => '1 week ago'
					)
				)
			));
		} elseif(is_single()) {

			$instance['title'] = '<h2 class="title">Read More</h2>';
			$article_list = new WP_Query(array(
				'posts_per_page' => 3,
				'post__not_in' => $post_ids_for_widget,
				'category__in' => $cats_for_widget
			));



		} else {
			$instance['title'] = '<h2 class="title">Ad Features</h2>';
			$is_ad_feature = true;
			$article_list = new WP_Query(array(
				'posts_per_page' => 3,
				'meta_value' => '1',
				'meta_key' => 'is_ad_feature'
			));
		}

		//var_dump($article_list);

		$post_count = 0;

		if ( $article_list->have_posts() ) {
			echo $instance['title'];
			echo '<div class="article_list widget">';

			while ( $article_list->have_posts() ) {
				$article_list->the_post();
				$cat_count = 0;
				$cats = get_the_terms($article_list->post->ID, 'category');


				echo '<div class="article_list_item">';

				if( ++$post_count == 1 and has_post_thumbnail($article_list->post->ID) ) {
					echo get_the_post_thumbnail( $article_list->post->ID, 'tr-small' );
				} elseif($post_count == 1) {
					echo '<img src="'.get_stylesheet_directory_uri().'/includes/img/no-image-200x150.png" alt="No Image" />';
				}

				if( !empty($cats) ) {
					echo '<p>';

					if( !$is_ad_feature and get_post_meta($post->ID,'is_ad_feature',true) == 1) {
						echo '<span class="term term_ad_feature">Ad Feature</span> ';
						$cat_count++;
					}

					foreach($cats as $cat ) if( $cat->parent == 0 ) {
						if($cat_count++ > 2) continue;
						echo '<span class="term">'.$cat->name.'</span>';
					}
					foreach($cats as $cat ) if( $cat->parent != 0 ) {
						if( ++$cat_count > 2) continue;
						echo '<span class="term">'.$cat->name.'</span>';
					}
					echo '</p>';
				}

				echo '<a href="'.get_permalink().'">' . get_the_title( $article_list->post->ID ) . '</a>';

				echo '</div>';

			}
			echo '</div>';
		} // end have posts
		wp_reset_query();
		$post = $oldpost;

 	}
	public function form($instance) { ?>

		<input type="hidden" id="<?php echo $this->get_field_id( 'title' ); ?>" value=" <?php if ($instance['type'] == 'tranding') echo 'Tranding'; else echo 'Related/Sponsored'; ?>" >

		<p>
			<label>
				<input type="radio" name="<?php echo $this->get_field_name( 'type' ); ?>"
				id="<?php echo $this->get_field_id( 'type' ); ?>" value="tranding" <?php if ($instance['type'] == 'tranding') echo 'checked="checked"'; ?>>
				Tranding <span style="color: #ccc">(most read)</span>
			</label><br />
			<label>
				<input type="radio" name="<?php echo $this->get_field_name( 'type' ); ?>"
				id="<?php echo $this->get_field_id( 'type' ); ?>" value="related_sponsored" <?php if ($instance['type'] == 'related_sponsored') echo 'checked="checked"'; ?>>
				Related or Sponsored
			</label>
		</p>

	<?php
	}
}

class html extends WP_Widget {
	function __construct() {
		parent::__construct( 'html', 'HTML',
			array( 'description' => 'For custom HTML')
		);
	}
	public function widget( $args, $instance ) {
		echo '<div class="widget html">'.$instance['html'].'</div>';
	}
	public function form( $instance ) { ?>
		<input type="hidden" name="widget-width" class="widget-width" value="400">
		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>">Title <span class="gray">(reference only)</span></label><br>
			<input class="widefat" type="text" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php echo $instance[ 'title' ]; ?>" id="<?php echo $this->get_field_id( 'title' ); ?>" placeholder="element title" >
		</p>

		<p>
			<label For="<?php echo $this->get_field_id( 'html' ); ?>">HTML</label><br>
			<textarea style="height:250px; width:100%;" name="<?php echo $this->get_field_name( 'html' ); ?>" id="<?php echo $this->get_field_id( 'html' ); ?>"><?php echo $instance[ 'html' ]; ?></textarea>
		</p>

	<?php
	}
}

class six_article_intro extends WP_Widget {
	function __construct() {
		parent::__construct( 'six_article_intro', 'Six Article Intro',
			array( 'description' => 'Display six articles, build for the homepage')
		);
	}
	public function widget( $args, $instance ) {

		global $post;
		$oldpost = $post;

		$articles = new WP_Query(array(
			'posts_per_page' => 6
		));

		$post_count = 0;

		if ( $articles->have_posts() ) {
			echo '<div class="widget six_article_intro clear">';

			while ( $articles->have_posts() ) {
				$articles->the_post();

				if(++$post_count == 1) $class = ' width_100';
				elseif($post_count > 1 and $post_count < 4) $class = ' width_33';
				elseif($post_count > 4) $class = ' width_50';

				if( ! has_post_thumbnail()) $featured_image[0] = get_stylesheet_directory_uri().'/includes/img/no-image-720x620.png';
				else {
					if($post_count == 1) $featured_image = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID),'tr-feature');
					else $featured_image = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID),'tr-large');
				}

				if(get_post_meta($post->ID,'is_ad_feature',true) == 1) $ad_feature = '<strong class="ad_feature">Ad Feature</strong> ';
				else $ad_feature = '';

				echo '<a class="article_item post_'. $post_count . $class . '" style="background-image: url('.$featured_image[0].')" href="'.get_permalink().'"><span>' . $ad_feature . get_the_title( $article_list->post->ID ) . '</span></a>';

			}
			echo '</div>';
		}
		wp_reset_query();
		$post = $oldpost;



	}
	public function form( $instance ) { ?>
		<p>This are no setting for this widget.</p>
	<?php
	}
}

class large_article_list extends WP_Widget {
	function __construct() {
		parent::__construct( 'large_article_list', 'Large Article List',
			array( 'description' => 'Display articles in their Columns built for the homepage')
		);
	}
	public function widget( $args, $instance ) {

		global $post;
		$oldpost = $post;

		$articles = new WP_Query(array(
			'posts_per_page' => empty($instance['placement_id'])?11:8,
			'category_name' => $instance['cat_name']
		));

		$post_count = 0;

		if ( $articles->have_posts() ) {
			echo '<div class="widget large_article_list clear">';
			if( !empty($instance['title']) ) echo '<h2  class="main_title" >'.$instance['title'].'</h2>';

			while ( $articles->have_posts() ) {
				$articles->the_post();
				if(get_post_meta($post->ID,'is_ad_feature',true) == 1) $ad_feature = '<strong class="ad_feature">Ad Feature</strong> ';
				else $ad_feature = '';

				if(++$post_count == 1) { ?>

					<div class="col col_1">
						<a class="article_item_large" href="<?php echo get_permalink(); ?>">
							<?php if(has_post_thumbnail()) the_post_thumbnail('tr-large');
							else echo '<img src="'.get_stylesheet_directory_uri().'/includes/img/no-image-720x620.png" alt="No Image" />'; ?>
							<h4><?php echo $ad_feature . get_the_title( $article_list->post->ID ); ?></h4>
							<?php the_excerpt() ?>
						</a>
					</div>

				<?php } else {

					if($post_count == 2) echo '<div class="col col_2">';
					if($post_count == 6) echo '<div class="col col_3'. ( empty($instance['placement_id'])?'':' has_advert').'">';

					if($post_count == 6 and !empty($instance['placement_id']) ) {

					//	the_widget('render_adtech_advert', array(
					//		'adtech_type' => '170',
					//		'adtech_id' => $instance['placement_id']
					
					if($instance['placement_id'] == 1) 
					echo '<!-- DFP -->
						 <div class="advert">
							<div id="ad-slot-mpu-1-desktop"> 
						        <script>
									if (window.innerWidth >= 1024) {
										googletag.cmd.push(function() { 
											googletag.display("ad-slot-mpu-1-desktop"); 
										});
									}
						        </script> 
						    </div> 
						    <div id="ad-slot-mpu-1-tablet"> 
						        <script>
									if (window.innerWidth >= 728 && window.innerWidth < 1024) {
										googletag.cmd.push(function() { 
											googletag.display("ad-slot-mpu-1-tablet"); 
										});
									}
						        </script> 
						    </div> 
						    <div id="ad-slot-mpu-1-mobile"> 
						        <script>
									if (window.innerWidth < 728) {
										googletag.cmd.push(function() { 
											googletag.display("ad-slot-mpu-1-mobile"); 
										});
									}
						        </script> 
						    </div> 
						</div>';

                                       if($instance['placement_id'] == 2)
                                        echo '<div class="advert">
													<div id="ad-slot-mpu-2-desktop"> 
														<script>
															if (window.innerWidth >= 1024) {
																googletag.cmd.push(function() { 
																	googletag.display("ad-slot-mpu-2-desktop"); 
																});
															}
														</script> 
													</div> 
													<div id="ad-slot-mpu-2-tablet"> 
														<script>
															if (window.innerWidth >= 728 && window.innerWidth < 1024) {
																googletag.cmd.push(function() { 
																	googletag.display("ad-slot-mpu-2-tablet"); 
																});
															}
														</script> 
													</div> 
													<div id="ad-slot-mpu-2-mobile"> 
														<script>
															if (window.innerWidth < 728) {
																googletag.cmd.push(function() { 
																	googletag.display("ad-slot-mpu-2-mobile"); 
																});
															}
														</script> 
													</div> 
												</div>';
					


					}
					?>

					<a class="article_item" href="<?php echo get_permalink(); ?>">
						<?php if($post_count > 1 and $post_count < 6 and has_post_thumbnail()) the_post_thumbnail('tr-small'); ?>
						<h4><?php echo $ad_feature . get_the_title( $article_list->post->ID ); ?></h4>
					</a>

					<?php
					if($post_count == 5 or $post_count == count($articles->posts)) echo '</div>';

				}


			}
			echo '</div>';
		}
		wp_reset_query();

		$post = $oldpost;



	}
	public function form( $instance ) { ?>

		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>">Title:</label><br>
			<input class="widefat" type="text" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php echo $instance[ 'title' ]; ?>" id="<?php echo $this->get_field_id( 'title' ); ?>" placeholder="widget title" >
		</p>

		<p>
			<label for="<?php echo $this->get_field_id( 'cat_name' ); ?>">Category Slug:</label><br>
			<input class="widefat" type="text" name="<?php echo $this->get_field_name( 'cat_name' ); ?>" value="<?php echo $instance[ 'cat_name' ]; ?>" id="<?php echo $this->get_field_id( 'cat_name' ); ?>" placeholder="category slug" >
		</p>

		<p>
			<label for="<?php echo $this->get_field_id( 'placement_id' ); ?>">Advert ID: <span style="color: #ccc">(leave blank for no advert)</span></label><br>
			<input class="widefat" type="text" name="<?php echo $this->get_field_name( 'placement_id' ); ?>" value="<?php echo $instance[ 'placement_id' ]; ?>" id="<?php echo $this->get_field_id( 'placement_id' ); ?>" placeholder="placement id" >
		</p>

	<?php
	}
}


class small_article_list extends WP_Widget {
	function __construct() {
		parent::__construct( 'small_article_list', 'Small Article List x3',
			array( 'description' => 'Display articles in single Columns, should be in 3x build for the homepage')
		);
	}
	public function widget( $args, $instance ) {

		global $post;
		$oldpost = $post;

		echo '<div class="widget small_article_list clear">';
		foreach(range(1,3) as $i) {

			$articles = new WP_Query(array(
				'posts_per_page' => 4,
				'category_name' => $instance['cat_name_'.$i]
			));

			$post_count = 0;

			if ( $articles->have_posts() ) {
				echo '<div class="small_article_list_col small_article_list_'.$i.'">';
				if( !empty($instance['title_'.$i]) ) echo '<h2  class="main_title">'.$instance['title_'.$i].'</h2>';

				while ( $articles->have_posts() ) {
					$articles->the_post();
					if(get_post_meta($post->ID,'is_ad_feature',true) == 1) $ad_feature = '<strong class="ad_feature">Ad Feature</strong> ';
					else $ad_feature = '';

					if(++$post_count == 1) { ?>

						<a class="article_item_large" href="<?php echo get_permalink(); ?>">
							<?php if(has_post_thumbnail()) the_post_thumbnail('tr-main');
							else echo '<img src="'.get_stylesheet_directory_uri().'/includes/img/no-image-600x400.png" alt="No Image" />'; ?>
							<h4><?php echo $ad_feature . get_the_title( $article_list->post->ID ); ?></h4>
							<?php the_excerpt() ?>
						</a>

					<?php } else { ?>

						<a class="article_item" href="<?php echo get_permalink(); ?>">
							<?php if(has_post_thumbnail()) the_post_thumbnail('tr-small'); ?>
							<h4><?php echo $ad_feature . get_the_title( $article_list->post->ID ); ?></h4>
						</a>

					<?php }

				}
				echo '</div>';
			}
			wp_reset_query();

		}

		echo '</div>';

		$post = $oldpost;

	}
	public function form( $instance ) { ?>

		<p>
			<label for="<?php echo $this->get_field_id( 'title_1' ); ?>"><strong>One</strong> | Title:</label><br>
			<input class="widefat" type="text" name="<?php echo $this->get_field_name( 'title_1' ); ?>" value="<?php echo $instance[ 'title_1' ]; ?>" id="<?php echo $this->get_field_id( 'title_1' ); ?>" placeholder="widget title" >
		</p>

		<p>
			<label for="<?php echo $this->get_field_id( 'cat_name_1' ); ?>"><strong>One</strong> | Category Slug:</label><br>
			<input class="widefat" type="text" name="<?php echo $this->get_field_name( 'cat_name_1' ); ?>" value="<?php echo $instance[ 'cat_name_1' ]; ?>" id="<?php echo $this->get_field_id( 'cat_name_1' ); ?>" placeholder="category slug" >
		</p>
		<hr style="margin: 20px 0 15px 0;" />
		<p>
			<label for="<?php echo $this->get_field_id( 'title_2' ); ?>"><strong>Two</strong> | Title:</label><br>
			<input class="widefat" type="text" name="<?php echo $this->get_field_name( 'title_2' ); ?>" value="<?php echo $instance[ 'title_2' ]; ?>" id="<?php echo $this->get_field_id( 'title_2' ); ?>" placeholder="widget title" >
		</p>

		<p>
			<label for="<?php echo $this->get_field_id( 'cat_name_2' ); ?>"><strong>Two</strong> | Category Slug:</label><br>
			<input class="widefat" type="text" name="<?php echo $this->get_field_name( 'cat_name_2' ); ?>" value="<?php echo $instance[ 'cat_name_2' ]; ?>" id="<?php echo $this->get_field_id( 'cat_name_2' ); ?>" placeholder="category slug" >
		</p>
		<hr style="margin: 20px 0 15px 0;" />
		<p>
			<label for="<?php echo $this->get_field_id( 'title_3' ); ?>"><strong>Three</strong> | Title:</label><br>
			<input class="widefat" type="text" name="<?php echo $this->get_field_name( 'title_3' ); ?>" value="<?php echo $instance[ 'title_3' ]; ?>" id="<?php echo $this->get_field_id( 'title_3' ); ?>" placeholder="widget title" >
		</p>

		<p>
			<label for="<?php echo $this->get_field_id( 'cat_name_3' ); ?>"><strong>Three</strong> | Category Slug:</label><br>
			<input class="widefat" type="text" name="<?php echo $this->get_field_name( 'cat_name_3' ); ?>" value="<?php echo $instance[ 'cat_name_3' ]; ?>" id="<?php echo $this->get_field_id( 'cat_name_3' ); ?>" placeholder="category slug" >
		</p>

	<?php
	}
}


class platinum_article_list extends WP_Widget {
	function __construct() {
		parent::__construct( 'platinum_article_list', 'Platinum Article List',
			array( 'description' => 'Display articles in 3 Columns, has platinum styling')
		);
	}
	public function widget( $args, $instance ) {

		global $post;
		$oldpost = $post;

		echo '<div class="widget platinum_article_list clear"><div class="platinum_article_list_inner">';
		if( !empty($instance['title']) ) echo '<h2 class="main_title light">'.$instance['title'].'</h2>';

			$articles = new WP_Query(array(
				'posts_per_page' => 7,
				'category_name' => $instance['cat_name']
			));

			$post_count = 0;

			if ( $articles->have_posts() ) {

				while ( $articles->have_posts() ) {
					$articles->the_post();
					if(get_post_meta($post->ID,'is_ad_feature',true) == 1) $ad_feature = '<strong class="ad_feature">Ad Feature</strong> ';
					else $ad_feature = '';

					if(++$post_count == 1) { ?>

						<div class="platinum_article_list_col">
							<a class="article_item_large" href="<?php echo get_permalink(); ?>">
								<?php if(has_post_thumbnail()) the_post_thumbnail('tr-large');
								else echo '<img src="'.get_stylesheet_directory_uri().'/includes/img/no-image-720x620.png" alt="No Image" />'; ?>
								<h4><?php echo $ad_feature . get_the_title( $article_list->post->ID ); ?></h4>
							</a>
						</div>

					<?php } else { if($post_count == 2 or $post_count == 5) echo '<div class="platinum_article_list_col">'; ?>

						<a class="article_item" href="<?php echo get_permalink(); ?>">
							<?php if( has_post_thumbnail() ) the_post_thumbnail('tr-small'); ?>
							<h4><?php echo $ad_feature . get_the_title( $article_list->post->ID ); ?></h4>
						</a>

						<?php if($post_count == 4 or $post_count == 7) echo '</div>';
					}

				}
				echo '</div>';
			}
			wp_reset_query();

		echo '</div></div>';

		$post = $oldpost;

	}
	public function form( $instance ) { ?>

		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>">Title:</label><br>
			<input class="widefat" type="text" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php echo $instance[ 'title' ]; ?>" id="<?php echo $this->get_field_id( 'title' ); ?>" placeholder="widget title" >
		</p>

		<p>
			<label for="<?php echo $this->get_field_id( 'cat_name' ); ?>">Category Slug:</label><br>
			<input class="widefat" type="text" name="<?php echo $this->get_field_name( 'cat_name' ); ?>" value="<?php echo $instance[ 'cat_name' ]; ?>" id="<?php echo $this->get_field_id( 'cat_name' ); ?>" placeholder="category slug" >
		</p>

	<?php
	}
}

class sliding_article_list extends WP_Widget {
	function __construct() {
		parent::__construct( 'sliding_article_list', 'Sliding Article List',
			array( 'description' => 'Display 9 articles in sliding strip')
		);
	}
	public function widget( $args, $instance ) {

		global $post;
		$oldpost = $post;

		echo '<div id="'.$this->id.'" class="widget sliding_article_list clear transitions"><div class="sliding_article_list_inner">';
		if( !empty($instance['title']) ) echo '<h2 class="main_title light">'.$instance['title'].'</h2>';

			$articles = new WP_Query(array(
				'posts_per_page' => 9,
				'category_name' => $instance['cat_name']
			));

			if ( $articles->have_posts() ) {

				echo '<div class="sliding_article_list_holder" style="left:-340px;">';

				while ( $articles->have_posts() ) {
					$articles->the_post();
					if(get_post_meta($post->ID,'is_ad_feature',true) == 1) $ad_feature = '<strong class="ad_feature">Ad Feature</strong> ';
					else $ad_feature = ''; ?>

						<a class="article_item_large" href="<?php echo get_permalink(); ?>">
							<?php if(has_post_thumbnail()) the_post_thumbnail('tr-large');
							else echo '<img src="'.get_stylesheet_directory_uri().'/includes/img/no-image-720x620.png" alt="No Image" />'; ?>
							<h4><?php echo $ad_feature . get_the_title( $article_list->post->ID ); ?></h4>
						</a>

				<?php }

				echo '</div>';

			}
			wp_reset_query();

			echo '<button class="sliding_article_left">Left</button>';
			echo '<button class="sliding_article_right">Right</button>';

		echo '</div></div>';

		$post = $oldpost;

	}
	public function form( $instance ) { ?>

		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>">Title:</label><br>
			<input class="widefat" type="text" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php echo $instance[ 'title' ]; ?>" id="<?php echo $this->get_field_id( 'title' ); ?>" placeholder="widget title" >
		</p>

		<p>
			<label for="<?php echo $this->get_field_id( 'cat_name' ); ?>">Category Slug:</label><br>
			<input class="widefat" type="text" name="<?php echo $this->get_field_name( 'cat_name' ); ?>" value="<?php echo $instance[ 'cat_name' ]; ?>" id="<?php echo $this->get_field_id( 'cat_name' ); ?>" placeholder="category slug" >
		</p>

	<?php
	}
}

class render_adtech_advert extends WP_Widget {

	function __construct() {
		parent::__construct( 'render_adtech_advert', 'Render Adtech Advert',
			array( 'description' => "Renders an Advert out on the website")
		);
	}

	public static $types = array(
			'170' => array('300','250','mpu'),
			'225' => array('728','90','leaderborad')
	);


	public function widget($args,$instance) {
		$instance['adtech_type'] = trim($instance['adtech_type']);
		$instance['adtech_id'] = trim($instance['adtech_id']);
	?>

	<div id="<?php echo $args['widget_id']; ?>" class="widget ad ad_<?php echo $instance['adtech_type']; ?>">
		<?php if($instance['show_ad_label']){ ?><span class="title">ADVERTISEMENT</span><?php } ?>

		<script language="javascript">
		<!--
		if (window.adgroupid == undefined) {
			window.adgroupid = Math.round(Math.random() * 1000);
		}
		var placement_id = '<?php echo $instance['adtech_id']; ?>';
		var tablet_id = '<?php echo $instance['tablet_id']; ?>';
		var mobile_id = '<?php echo $instance['mobile_id']; ?>';

		if(placement_id && tablet_id && mobile_id){

			var w = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;

			if(w < 728) placement_id = mobile_id;
				else if(w <= 1000) placement_id = tablet_id;
		}

		document.write('<scr'+'ipt language="javascript1.1" src="https://adserver.adtech.de/addyn/3.0/1482.1/'+placement_id+'/0/-1/ADTECH;cookie=info;loc=100;target=_blank;key=<?php echo $_SERVER['REQUEST_URI']; ?>;grp='+window.adgroupid+';misc='+new Date().getTime()+'"></scri'+'pt>');
		//-->
		</script><noscript><a href="https://adserver.adtech.de/adlink/3.0/1482/<?php echo $instance['adtech_id']; ?>/0/-1/ADTECH;loc=300;key=<?php echo $_SERVER['REQUEST_URI']; ?>" target="_blank"><img src="https://adserver.adtech.de/adserv/3.0/1482/<?php echo $instance['adtech_id']; ?>/0/<?php echo $instance['adtech_type']; ?>/ADTECH;cookie=info;loc=300;key=<?php echo $_SERVER['REQUEST_URI']; ?>" border="0" width="<?php echo render_adtech_advert::$types[$instance['adtech_type']][0]; ?>" height="<?php echo render_adtech_advert::$types[$instance['adtech_type']][1]; ?>"></a></noscript>

	</div>

	<?php
	}
	public function form( $instance ) { ?>

		<p>
			<label for="<?php echo $this->get_field_id( 'adtech_id' ); ?>">Desktop ID:</label>
			<input class="widefat" name="<?php echo $this->get_field_name( 'adtech_id' ); ?>" type="text" value="<?php echo $instance[ 'adtech_id' ]; ?>">
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'tablet_id' ); ?>">Tablet ID:</label>
			<input class="widefat" name="<?php echo $this->get_field_name( 'tablet_id' ); ?>" type="text" value="<?php echo $instance[ 'tablet_id' ]; ?>">
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'mobile_id' ); ?>">Mobile ID:</label>
			<input class="widefat" name="<?php echo $this->get_field_name( 'mobile_id' ); ?>" type="text" value="<?php echo $instance[ 'mobile_id' ]; ?>">
		</p>
		<p>
		<label for="<?php echo $this->get_field_id( 'adtech_type' ); ?>">Advert Type:</label>
		<select class="widefat" name="<?php echo $this->get_field_name( 'adtech_type' ); ?>">
			<?php foreach(render_adtech_advert::$types as $type => $typeInfo)echo '<option '.($instance['adtech_type'] == $type?' selected':'').' value="'.$type.'">'.$typeInfo[2].'</option>'; ?>
		</select>
		</p>

	<?php
	}
}
