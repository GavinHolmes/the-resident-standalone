<?php

add_action( 'init', function() {
	$args = array(
		'label' => 'Recipes',
		'public' => false,
		'show_ui' => true,
		'supports' => array( 'title','thumbnail'),
		'menu_icon' => 'dashicons-media-text'
	);
	register_post_type( 'recipe', $args );
});



add_action( 'add_meta_boxes',function(){
	add_meta_box( 'recipe_meta_box','Recipe Settings', 'tr_recipe_meta_box_render', 'recipe','normal','high' );
	add_meta_box( 'tr-recipe-insert','Insert Recipe', 'tr_insert_recipe_meta_box_render', 'post','side', 'high');
});


function tr_recipe_meta_box_render() {

	global $post;
	$rescipe_meta = get_post_meta($post->ID);

	$details[] = array( 'title' => 'About', 'type' => 'heading');
	$details[] = array( 'title' => 'Author name',  'meta_name' => 'author', 'type' => 'text');
	$details[] = array( 'title' => 'Brief Summary', 'meta_name' => 'summary', 'type' => 'textarea');

	$details[] = array( 'title' => 'Details', 'type' => 'heading');
	$details[] = array( 'title' => 'Preparation Time', 'meta_name' => 'prepTime', 'type' => 'text');
	$details[] = array( 'title' => 'Cooking Time', 'meta_name' => 'cookTime', 'type' => 'text');
	$details[] = array( 'title' => 'Total Time', 'meta_name' => 'totalTime', 'type' => 'text');
	$details[] = array( 'title' => 'Servings', 'meta_name' => 'servings', 'type' => 'text');
	$details[] = array( 'title' => 'Yields', 'meta_name' => 'yields', 'type' => 'text');

	$details[] = array( 'title' => 'Reference or adapted', 'type' => 'heading');
	$details[] = array( 'title' => 'Name or Website for Reference', 'meta_name' => 'adapted', 'type' => 'text');
	$details[] = array( 'title' => 'http link <span class="gray">(must start with http://)</span>', 'meta_name' => 'adaptedLink', 'type' => 'text');

	$details[] = array( 'title' => 'The Recipe', 'type' => 'heading');
	$details[] = array( 'title' => 'The Ingredients',  'meta_name' => 'ingredients', 'type' => 'textarea');
	$details[] = array( 'title' => 'The Directions',  'meta_name' => 'directions', 'type' => 'textarea');
	$details[] = array( 'title' => 'Notes',  'meta_name' => 'notes', 'type' => 'textarea');

	foreach($details as $a) {

		if($a['type'] == 'heading') echo '<h3>'.$a['title'].'</h3>';

		if($a['type'] == 'text') {  ?>
	<label>
		<p><?php echo $a['title']; ?></p>
		<input type="text" name="meta[<?php echo $a['meta_name']; ?>]" class="regular-text" value="<?php echo $rescipe_meta[$a['meta_name']][0]; ?>" />
	</label>
	<?php }

	if($a['type'] == 'textarea') {  ?>
<label>
	<p><?php echo $a['title']; ?></p>
	<textarea name="meta[<?php echo $a['meta_name']; ?>]" rows="5" class="large-text code"><?php echo $rescipe_meta[$a['meta_name']][0]; ?></textarea>
</label>
<?php }
	}
}

function tr_insert_recipe_meta_box_render(){ ?>
	<select id="tr-recipe-select">
		<?php
		global $post;
		$oldpost = $post;
		$recipes = new WP_Query(array(
			'post_type' => 'recipe',
			'posts_per_page' => -1,
			'post_status' => 'publish'
		));
		while($recipes->have_posts()){
			$recipes->the_post(); ?>
			<option value="<?php echo get_the_ID(); ?>"><?php echo get_the_title(); ?></option>
		<?php }
		$post = $oldpost;
		wp_reset_postdata(); ?>
	</select><button type="button" class="button" onclick="send_to_editor('[recipe id=' + jQuery('#tr-recipe-select').val() + ']');">Add</button>
<?php }


add_shortcode('recipe', 'add_shortcode_recipe');

function add_shortcode_recipe($attributes){
	$recipe_id = $attributes['id'];
	$recipes = new WP_Query(array(
		'post_type' => 'recipe',
		'posts_per_page' => 1,
		'p' => $recipe_id
	));
	if(count($recipes->posts) === 0) return '';
	$recipes->the_post();
	$meta = get_post_meta(get_the_ID());
	ob_start();	?>

	<div class="recipe <?php echo $meta['summary'][0] ? 'has-summary' : ''; ?> <?php echo has_post_thumbnail(get_the_ID()) ? 'has-image' : '' ; ?>">
		<div class="recipe-image">
			<?php echo get_the_post_thumbnail(get_the_ID()); ?>
			<button class="print button" type="button" onclick="window.print();">Print</button>
		</div>
		<div class="inner">
			<div class="title"><h2><?php echo get_the_title(); ?></h2></div>
			<?php if($meta['summary'][0]){ ?><p><?php echo nl2br($meta['summary'][0]);?></p><?php }
			$info = array();
			if($meta['prepTime'][0]) 	$info['Prep Time'] = $meta['prepTime'][0];
			if($meta['cookTime'][0]) 	$info['Cook Time'] = $meta['cookTime'][0];
			if($meta['totalTime'][0]) $info['Total Time'] = $meta['totalTime'][0];
			if($meta['servings'][0]) 	$info['Serves'] = $meta['servings'][0];
			if($meta['yields'][0]) 		$info['Yields'] = $meta['yields'][0];

			if(count($info)){ ?>
					<ul class="info">
						<?php foreach($info as $key=>$val){ ?>
							<li><?php echo $key; ?> <span><?php echo $val; ?></span></li>
						<?php } ?>
					</ul>
			<?php }	?>

			<div class="body">
				<div class="ingredients">
					<h3>Ingredients</h3>
					<?php echo nl2br($meta['ingredients'][0]); ?>
				</div>

				<div class="method">
					<?php echo nl2br($meta['directions'][0]); ?>
				</div>
				<?php if($meta['author'][0]){ ?><p class="author">By <span><?php echo $meta['author'][0]; ?></span></p><?php } ?>
			</div>
		</div>
	</div>
	<?php
	$output = ob_get_clean();

	return $output;
};
