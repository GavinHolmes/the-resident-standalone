<?php

add_action('wp_footer', function() {

	if( !isset($_COOKIE['seen_newsletter_overlay']) or $_COOKIE['seen_newsletter_overlay'] < 3 ) { ?>

		<style>
			#newsletter_overlay_wrapper {
				background: transparent;
				position: fixed;
				top: 0;
				left: 0;
				width: 100%;
				transition: background 0.5s;
				z-index: 4;
				display: none;
			}
			#newsletter_overlay_wrapper.display {
				background: rgba(0, 0, 0, 0.75);
				display: block;
			}
			#newsletter_overlay {
				position: fixed;
				top: 180px;
				left: 50%;
				width: 460px;
				margin: 0 0 0 -260px;
				background: #fff;
				box-shadow: 0 0 15px rgba(0, 0, 0, 0.5);
				padding: 40px;
				opacity: 0;
				transition: opacity 0.2s;
				height: 100%;   
				overflow-y : scroll;
			}
			#newsletter_overlay.display {
				opacity: 1;
			}
			#newsletter_overlay button {
				position: absolute;
				border: none;
				background: #d1ad54;
				color: #fff;
				padding: 6px 12px;
				font-size: 12px;
				top: 0;
				right: 0;
				cursor: pointer;
			}
			#newsletter_overlay h2 {
				font-family: 'Playfair Display', serif;
				font-weight: 400;
				font-size: 40px;
				text-transform: uppercase;
			}
			#newsletter_overlay p {
				font-size: 18px;
				margin: 0 0 20px 0;
			}
			#newsletter_overlay form {
				padding-bottom: 50px;
				position: relative;
			}
			#newsletter_overlay .text {
				border: none;
				background: #f1f1f1;
				padding: 13px 15px;
				width: calc(100% - 30px);
				font-size: 18px;
				font-weight: 300;
				margin-bottom: 20px;
			}
			#newsletter_overlay .submit {
				position: absolute;
				border: none;
				-webkit-appearance: none;
				border-radius: 0;
				padding: 0 25px;
				height: 45px;
				line-height: 45px;
				position: absolute;
				right: 0;
				bottom: 0;
				color: #fff;
				background: #d1ad54;
				font-size: 18px;
				font-weight: 500;
			}
			.display_none #newsletter_overlay, #newsletter_overlay_wrapper.display_none {
				display: none;
			}

			@media (max-width: 700px) {

				#newsletter_overlay {
					left: auto;
					top: 100px;
					width: calc(100% - 80px);
					margin: 0 20px;
					padding: 30px 20px;
				}
				#newsletter_overlay h2 {
					font-size: 20px;
				}
				#newsletter_overlay p {
					font-size: 14px;
				}

			}


		</style>

	<div id="newsletter_overlay_wrapper">

		<div id="newsletter_overlay">
			<button onclick="close_newsletter_overlay();">close</button>
			<h2>Like what you see?</h2>
			<p>Sign up to The Resident newsletter for even more news, views and things to do in London, delivered direct to your inbox once a week</p>
			<?php echo do_shortcode('[resident_newsletter]'); ?>
		</div>

	</div>

	<script>
		var accept_cookies_viewed = parseInt(readCookie('seen_newsletter_overlay'));

		var wrapper = document.getElementById('newsletter_overlay_wrapper');
		wrapper.addEventListener('click', trNewsletterOverlayClick);

		function trNewsletterOverlayClick(e){
			if(e.target.id == 'newsletter_overlay_wrapper') close_newsletter_overlay();
		}

		if(isNaN(accept_cookies_viewed)) createCookie('seen_newsletter_overlay','1',200);
		else if(accept_cookies_viewed <= 1) createCookie('seen_newsletter_overlay', accept_cookies_viewed + 1 ,200);
		else if(accept_cookies_viewed == 2) {

			window.addEventListener('load',function(){

				window.setTimeout(function(){
					document.getElementById('newsletter_overlay_wrapper').setAttribute('style','height: '+window.innerHeight+'px');
					document.getElementById('newsletter_overlay_wrapper').className = 'display';
					//document.getElementById('newsletter_overlay').setAttribute('style','display: block');
				}, 1500);

				window.setTimeout(function(){
					document.getElementById('newsletter_overlay').className = 'display';
					setTimeout(trNewsletterResize, 2000);
				}, 1800);

			});

			window.addEventListener('resize', trNewsletterResize);
			window.addEventListener('scroll', trNewsletterResize);

		}



		function trNewsletterResize(){				
				// CH Dec 2017 - Change to fix newsletter overlay on mobile - Incident 305136 //
				document.getElementById('newsletter_overlay').setAttribute('style','top: 1%');
				//old method//document.getElementById('newsletter_overlay').setAttribute('style','top: '+ (document.documentElement.clientHeight - document.getElementById('newsletter_overlay').offsetHeight) / 40 +'px');
				document.getElementById('newsletter_overlay_wrapper').setAttribute('style','height: '+window.innerHeight+'px');
		}

		function createCookie(name,value,days) {
			if (days) {
				var date = new Date();
				date.setTime(date.getTime()+(days*24*60*60*1000));
				var expires = "; expires="+date.toGMTString();
			}
			else var expires = "";
			document.cookie = name+"="+value+expires+"; path=/";
		}

		function readCookie(name) {
			var nameEQ = name + "=";
			var ca = document.cookie.split(';');
			for(var i=0;i < ca.length;i++) {
				var c = ca[i];
				while (c.charAt(0)==' ') c = c.substring(1,c.length);
				if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
			}
			return null;
		}

		function close_newsletter_overlay() {
			createCookie('seen_newsletter_overlay','3',120);
			document.getElementById('newsletter_overlay_wrapper').className = 'display_none';
		}

	</script>

	<?php  }

});
