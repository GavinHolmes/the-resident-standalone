<?php

function tr_get_page_title(){
	global $post, $paged, $_su_title;
	$title = '';
	if(is_search()) {
		$title .= 'Search | ';
		$title .= get_bloginfo( 'name' );
		if ( $paged >= 2 || $page >= 2 ) $title .= ' | ' . sprintf( 'Page %s', max( $paged, $page ) );
	} else if(is_tag()) {
		if(get_term_meta(get_queried_object()->term_id, 'seo_title', true)){
			$title .= get_term_meta(get_queried_object()->term_id, 'seo_title', true);
		} else {
			$title .= single_tag_title( '', false ) .' | ';
			$title .= get_bloginfo( 'name' );
		}
		if ( $paged >= 2 || $page >= 2 ) $title .= ' | ' . sprintf( 'Page %s', max( $paged, $page ) );
	} else if(is_category()) {
		if(get_term_meta(get_queried_object()->term_id, 'seo_title', true)){
			$title .= get_term_meta(get_queried_object()->term_id, 'seo_title', true);
		} else {
			$title .= single_cat_title( '', false ) .' | ';
			$title .= get_bloginfo( 'name' );
		}
		if ( $paged >= 2 || $page >= 2 ) $title .= ' | ' . sprintf( 'Page %s', max( $paged, $page ) );
	} else {
		// Pages - check for seo meta first
		if(get_post_meta($post->ID, '_su_title', true)){
			$title .= get_post_meta($post->ID, '_su_title', true);
		} else {
			wp_title( '|', true, 'right' );
			$title .= get_bloginfo( 'name' );
			$site_description = get_bloginfo( 'description', 'display' );
			if ( $site_description && ( is_home() || is_front_page() ) ) $title .= " | $site_description";
			if ( $paged >= 2 || $page >= 2 ) $title .= ' | ' . sprintf( 'Page %s', max( $paged, $page ) );
		}
	}
	return $title;
}

function tr_get_description_meta(){
	global $post;
	if(is_tag() || is_category()){
		return get_term_meta(get_queried_object()->term_id, 'seo_description', true);
	} else return get_post_meta($post->ID, '_su_description', true);
}

add_action('category_add_form_fields', function() { ?>
	<div class="form-field">
		<label for="seo_title">Page title (SEO)</label>
		<input type="text" name="meta[seo_title]" id="seo_title" value="">
		<p>This will be used as the title for the archive page. Leave blank to use default</p>
	</div>
	<div class="form-field">
		<label for="seo_description">Page description (SEO)</label>
		<textarea name="meta[seo_description]" id="seo_description"></textarea>
		<p>This will be used as the meta description for the archive page. Leave blank to use default</p>
	</div>
<?php });

add_action('category_edit_form_fields', function($term) { ?>
	<tr class="form-field">
		<th scope="row"><label for="seo_title">Page title (SEO)</label></th>
		<td>
			<input name="meta[seo_title]" id="seo_title" type="text" value="<?php echo get_term_meta($term->term_id,'seo_title',true); ?>">
			<p class="description">This will be used as the title for the archive page. Leave blank to use default</p>
		</td>
	</tr>
	<tr class="form-field">
		<th scope="row"><label for="seo_description">Page description (SEO)</label></th>
		<td>
			<textarea name="meta[seo_description]" id="seo_description"><?php echo get_term_meta($term->term_id,'seo_description',true); ?></textarea>
			<p class="description">This will be used as the title for the archive page. Leave blank to use default</p>
		</td>
	</tr>
<?php });

function tr_update_term_meta($term_id) {
	foreach($_POST['meta'] as $key=>$val){
		update_term_meta($term_id, $key, $val );
	}
}
add_action( 'create_category', 'tr_update_term_meta');
add_action( 'edit_category', 'tr_update_term_meta' );
add_action( 'create_tag', 'tr_update_term_meta');
add_action( 'edit_tag', 'tr_update_term_meta' );
