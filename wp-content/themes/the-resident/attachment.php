<?php
get_header(); ?>

<div id="title-strip">
	<h2>attachment</h2>
</div>

<div class="content">

	<div class="left">
	<?php



	if ( have_posts() ) while ( have_posts() ) {
		the_post();
		echo '<h2>'.get_the_title().'</h2>';
		echo wp_get_attachment_image( get_the_ID(), 'large' );
		the_content();
	} else echo '<p>no posts</p>';
	?>
	</div>

<?php get_sidebar(); ?>

</div><!-- .content -->

<?php get_footer(); ?>
