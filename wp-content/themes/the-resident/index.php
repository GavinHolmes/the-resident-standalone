<?php
header("HTTP/1.0 404 Not Found");
get_header(); 
?>

<div id="title-strip">
	<h2>error</h2>
</div>

<div id="content" class="error-page">

<div class="left">

	<h1>Template Error</h1>
	<p>You have come to this page in error.</p>
	<?php mail('james.grimwood@archant.co.uk', 'Template error: '.$_SERVER['HTTP_HOST'], var_export ($_SERVER, true)); ?>

</div>

</div><!-- .content -->
<?php get_footer(); ?>
