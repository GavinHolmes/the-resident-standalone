<?php get_header(); ?>

<div class="author_strip"><div class="author_strip_inner clear"><?php
	$current_author = get_user_by('slug', $author_name);
	$current_author_description = get_user_meta(intval($author), 'description',true);

	echo get_avatar( $current_author->data->email, 160, $default );
	echo '<h2>'.$current_author->data->display_name.' <span>| Author</span></h2>';
	echo '<p>'.$current_author_description.'</p>';

?></div></div>


<div class="content">


	<div class="left">
		<div class="author_box">Viewing only <strong><?php echo $current_author->data->display_name; ?>’s</strong> articles</div>
	<?php
	if ( have_posts() ) while ( have_posts() ) {
		the_post(); ?>

		<div class="author_post clear">
			<div class="date"><span><?php echo the_time('M'); ?></span><?php echo the_time('j'); ?></div>
			<a href="<?php the_permalink(); ?>">
				<h4><?php echo $ad_feature.get_the_title(); ?></h4>
				<?php the_excerpt() ?>
			</a>
		</div>

		<?php
	} else echo '<p>no posts</p>';

	echo pagination();

	?>
	</div>

<?php get_sidebar(); ?>

</div><!-- .content -->

<?php get_footer(); ?>
