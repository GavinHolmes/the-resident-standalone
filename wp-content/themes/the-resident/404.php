<?php get_header(); ?>

<div id="title-strip">
	<h2>404</h2>
</div>

<div id="content" class="error-page">

<div class="left">

	<h1>Oops! we can’t find that</h1>
	<p>You may have followed a broken link, an outdated search result, or there may be an error on our site. If you typed in a URL, please make sure you have typed it in correctly. </p>

	<?php the_widget('tr_search_form'); ?>


</div>

</div><!-- .content -->
<?php get_footer(); ?>
