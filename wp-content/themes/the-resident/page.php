<?php
check_is_platinum($post->ID);
get_header(); ?>

<div id="title-strip">
	<h2><?php the_title(); ?></h2>
</div>

<div class="content">

	<div class="left">
	<?php
	if ( have_posts() ) while ( have_posts() ) {
		the_post();
		the_content();
	} else echo '<p>no posts</p>';
	?>
	</div>

<?php get_sidebar(); ?>

</div><!-- .content -->

<?php get_footer(); ?>
