<?php

add_shortcode( 'yumprint-recipe', 'add_shortcode_yumprint_recipe');
function add_shortcode_yumprint_recipe($atts,$content = ""){
	return '<strong title="yumprint"> ---- yumprint recipe ---- </strong> ';
};

include 'includes/php/run-of-site.php';
include 'includes/php/widgets.php';
include 'includes/php/post-type-recipe.php';
include 'includes/php/post-type-post.php';
include 'includes/php/post-type-page.php';
include 'includes/php/navigation.php';
include 'includes/php/read-more.php';
include 'includes/php/newsletter-overlay.php';
include 'includes/php/seo.php';
include 'includes/php/http-compression.php';

function add_newsletter_feed() {
	  	add_feed('newsletterfeed', 'newsletter_feed');
	}

add_action('init', 'add_newsletter_feed');

function newsletter_feed() {
  	add_filter('pre_option_rss_use_excerpt', '__return_zero');
  	load_template( TEMPLATEPATH . '/feeds/feed-newsletter.php' );
}