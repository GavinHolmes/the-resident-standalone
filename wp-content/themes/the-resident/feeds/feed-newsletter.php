<?php
/**
 * CSV Feed Template for Newsletters
 *
 * @package WordPress
 */
header('Content-Type: text/html');
$more = 1;
?>
"Publication","Categories","Title","Description","ImageUrl","Link","Published Date"
<?php
while (have_posts()) : the_post();
	$image = get_the_post_thumbnail_url($post_id, [600,400]);
	$categories = get_the_category();
	echo "\"TheResident\",";
	echo "\"[";
	foreach ($categories as $category){
		echo $category->name . ",";
	}
	echo "]\",\"";
	the_title_rss();
	echo "\",\"";
	the_excerpt_rss();
	echo "\",\"";
	echo $image;
	echo "\",\"";
	the_permalink_rss();
	echo "\",\"";
	the_date('dmY');
	echo "\"\n";
endwhile;
?>