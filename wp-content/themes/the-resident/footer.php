<div class="footer">
	<div class="top">
		<div class="top_inner">
			<div class="sign_up">  
	            <div class="widget html ">  
		            <h2 class="main_title">Sign up to our email newsletter</h2>
					<div>
						<div style="position: relative">
							<iframe id="subscriptionIframeFooter" width="" height="360" scrolling="no" frameborder="0" src="https://archant-mkt-prod1-m.adobe-campaign.com/lp/SUB_LMW" style="overflow: hidden;position: relative;height: 400px;/* width: 100%!important; */margin: auto;display: block;border: 1px #d0d0d0 solid;padding: 5px 0px;background: #ffffff;"></iframe>
							</div>
						</div>
					</div>
			</div>
		</div>
	</div>
	<div class="bottom">
		<div class="bottom_inner">
			<div class="right">

			<p>
				<strong>FOLLOW US:</strong>
				<a class="twitter" href="https://twitter.com/theresidentmag" target="_blank"></a>
				<a class="facebook" href="https://www.facebook.com/theresidentlondon" target="_blank"></a>
				<a class="instagram" href="https://www.instagram.com/theresidentlondon/" target="_blank"></a>
			</p>

			<?php
			if(current_user_can('Editor')) $items_wrap = '<ul id="%1$s" class="%2$s"><li><a href="/wp-admin/">Wordpress Admin</a></li>%3$s</ul>';
			else $items_wrap = '<ul id="%1$s" class="%2$s">%3$s</ul>';
			wp_nav_menu(array(
				'container' => '',
				'theme_location' => 'footer-menu',
				'menu_class' => 'footer-menu',
				'fallback_cb' => 'no_menu_set',
				'depth' => 1,
				'items_wrap' => $items_wrap
			));
			?>

		</div>

		<div class="left">
			<img src="<?php echo get_template_directory_uri(); ?>/includes/img/archant.svg" alt="Archant logo">
			<p>The Resident is brought to you by<br /> &copy; Archant Community Media <?php echo date('Y'); ?></p>
		</div>

	</div></div>

</div><!-- .footer -->

</div></div><!-- .wrap .shifter -->
					    <!-- Start DFP Wallpaper -->
						 <div class="advert"> 
						    <div id="ad-slot-wp-desktop"> 
						        <script>
									if (window.innerWidth >= 1024) {
										googletag.cmd.push(function() { 
											googletag.display("ad-slot-wp-desktop"); 
										});
									}
						        </script> 
						    </div>
						    <!--<div id="ad-slot-wp-tablet"> 
						        <script>
									if (window.innerWidth >= 728 && window.innerWidth < 1024) {
										googletag.cmd.push(function() { 
											googletag.display("ad-slot-wp-tablet"); 
										});
									}
						        </script> 
						    </div>
						    <div id="ad-slot-wp-mobile"> 
						        <script>
									if (window.innerWidth < 728) {
										googletag.cmd.push(function() { 
											googletag.display("ad-slot-wp-mobile"); 
										});
									}
						        </script> 
						    </div> -->
						</div>
						<!-- End DFP Wallpaper -->
						
						   <!-- Start DFP Sticky -->
						 <div class="advertFloat">
							<div id="ad-slot-float-desktop"> 
						        <script>
									if (window.innerWidth >= 1024) {
										googletag.cmd.push(function() { 
											googletag.display("ad-slot-wp-desktop"); 
										});
									}
						        </script> 
						    </div> 
						    <div id="ad-slot-float-tablet"> 
						        <script>
									if (window.innerWidth >= 728 && window.innerWidth < 1024) {
										googletag.cmd.push(function() { 
											googletag.display("ad-slot-wp-tablet"); 
										});
									}
						        </script> 
						    </div> 
						    <div id="ad-slot-float-mobile"> 
						        <script>
									if (window.innerWidth < 728) {
										googletag.cmd.push(function() { 
											googletag.display("ad-slot-wp-mobile"); 
										});
									}
						        </script> 
						    </div> 
						</div>
						 <!-- End DFP Sticky -->
<?php wp_footer(); ?>
</body>

<!-- Sourcepoint ReConsent Code -->
<style>
/* The Modal (background) */
.modal {
    display: none; /* Hidden by default */
    position: fixed; /* Stay in place */
    z-index: 1; /* Sit on top */
    left: 0;
    top: 0;
    width: 100%; /* Full width */
    height: 100%; /* Full height */
    overflow: auto; /* Enable scroll if needed */
    background-color: rgb(0,0,0); /* Fallback color */
    background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
}
/* Modal Content/Box */
.modal-content {
    background-color: #fefefe;
    margin: 15% auto; /* 15% from the top and centered */
    padding: 0px;
    border: 0px;
    width: 100%; /* Could be more or less, depending on screen size */
    height:600px;
	max-width: 600px;
}
</style>

<!-- The Modal -->
<div id="cmpModal" class="modal">

  <!-- Modal content -->
  <div class="modal-content">
	  <iframe id="cmpFrame" width="100%" height="600"></iframe>
  </div>

</div>

<script>
// Replace site ID for correct site - consent url is driven from current URL
var cmpFrameSrc = "//consent."+window.location.hostname.replace(/^www.?/g, "")+"/cmp/privacy_manager?privacy_manager_id=5b7168c0fc569a2775ec5f14&site_id=1132";
document.getElementById('cmpFrame').src = cmpFrameSrc;
</script>


<script>
// Get the modal
var modal = document.getElementById('cmpModal');
// Get the button that opens the modal
var btn = document.getElementById("cmpManageCookies");

// When the user clicks on the button, open the modal 
btn.onclick = function(event) {
  modal.style.display = "block";
  event.preventDefault();
}
// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
}

window.addEventListener('message', function (message) {
    var msgData = message.data;
    if (!msgData) return;
    if (!msgData.action || !(msgData.action === 'sp.complete' || msgData.action === 'sp.choiceComplete' || msgData.action === 'sp.cancel')) return;
    var iframeWindow = message && message.source;
    if (!iframeWindow) return;
    if (iframeWindow === window) {
      // we are on the privacy manager page so let's trigger a history navigation action here (going back in this case)
      window.history && window.history.go(-1);
      return;
    }
    var iframes = document.getElementsByTagName('iframe');
    var iframe;
    for (var i = 0; i < iframes.length; i++) {
      try { if (iframes[i].contentWindow === iframeWindow) iframe = iframes[i]; } catch (e) {}
    }
    if (!iframe) return;
    var parent = iframe.parentElement;
    if (!parent) return;
    // check to see if the sp_iframe_container is the parent which is a backhanded way of testing to see if this is being rendered by sp in which case we don't need to do anything
    if (window._sp_ && window._sp_.msg && window._sp_.msg.getMorphedClassName && window._sp_.msg.getMorphedClassName('sp_iframe_container') === parent.className) return;
    
    // here is where we trigger our action either removing iframe or hiding it or navigating back if this is a separate page or something
    //parent.removeChild(iframe);
    modal.style.display = "none";
});
</script>
</html>
