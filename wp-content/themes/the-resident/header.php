<?php global $tr_page_title; ?>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="referrer" content="origin-when-cross-origin">
<meta http-equiv="X-UA-Compatible" content="IE=Edge" />
	<title><?php echo $tr_page_title = tr_get_page_title();?></title>
	<?php $tr_description_meta = tr_get_description_meta();
	if($tr_description_meta){ ?><meta name="description" content="<?php echo substr($tr_description_meta,0,155); ?>" />
	<?php } ?>
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta property="og:type" content="website">
	<meta name="twitter:card" content="summary_large_image">
    
	<?php wp_head(); ?>
	<?php if(is_single() and get_post_type() == 'post') {?>
	<meta property="og:title" content="<?php echo get_the_title(); ?>" />
	<?php } ?>
    
<?php 	
    function full_url(){
        $s = empty($_SERVER["HTTPS"]) ? '' : ($_SERVER["HTTPS"] == "on") ? "s" : "";
        $protocol = substr(strtolower($_SERVER["SERVER_PROTOCOL"]), 0, strpos(strtolower($_SERVER["SERVER_PROTOCOL"]), "/")) . $s;
        $port = ($_SERVER["SERVER_PORT"] == "80") ? "" : (":".$_SERVER["SERVER_PORT"]);
        return $protocol . "://" . $_SERVER['SERVER_NAME'] . $port . $_SERVER['REQUEST_URI'];
    }

		//echo var_export($GLOBALS['post'], FALSE);
		$terms = get_the_terms($GLOBALS['post']->ID, 'category');
		$articlePage = false;
		if(count($terms) > 1){
			echo '<pre>';
			//echo var_export($GLOBALS, FALSE);
			//echo var_export($GLOBALS['post'], FALSE);
			//echo var_export($GLOBALS['post']->post_name, FALSE);
			echo '</pre>';
			foreach ( $terms as $term ) {
				 if ($term === end($terms)){
					$categoryTerms .= "'".$term->name."'";
					$categoryTermsSlug .= "'".$term->slug."'";
				}else{
					$categoryTerms .= "'".$term->name."',";	
					$categoryTermsSlug .= "'".$term->slug."',";						
					}
			}
			$articlePage = true;
		}else{
			//echo "no terms";
			$articlePage = false;
		}
	?>
		
<!-- dns prefetch start -->
<link rel="dns-prefetch" href="https://www.google-analytics.com">
<link rel="dns-prefetch" href="https://log.outbrainimg.com">
<link rel="dns-prefetch" href="https://tcheck.outbrainimg.com">
<link rel="dns-prefetch" href="https://mb.moatads.com">
<link rel="dns-prefetch" href="https://optimized-by.rubiconproject.com">
<link rel="dns-prefetch" href="https://sync.crwdcntrl.net">
<link rel="dns-prefetch" href="https://ad.crwdcntrl.net">
<link rel="dns-prefetch" href="https://securepubads.g.doubleclick.net">
<link rel="dns-prefetch" href="https://adservice.google.co.uk">
<link rel="dns-prefetch" href="https://adservice.google.com">
<link rel="dns-prefetch" href="https://consent.theresident.co.uk">
<link rel="dns-prefetch" href="https://sourcepoint.mgr.consensu.org">
<link rel="dns-prefetch" href="https://pm.sourcepoint.mgr.consensu.org">
<link rel="dns-prefetch" href="https://ads.rubiconproject.com">
<link rel="dns-prefetch" href="https://www.googletagservices.com">
<link rel="dns-prefetch" href="https://www.gstatic.com">
<link rel="dns-prefetch" href="https://px.moatads.com">
<link rel="dns-prefetch" href="https://loadeu.exelator.com">
<link rel="dns-prefetch" href="https://googleads.g.doubleclick.net">
<link rel="dns-prefetch" href="https://pagead2.googlesyndication.com">
<link rel="dns-prefetch" href="https://pp.lp4.io">
<link rel="dns-prefetch" href="https://googleads4.g.doubleclick.net">

<!-- dns prefetch end -->

<script type="text/javascript" src="//pp.lp4.io/app/57/f3/a8/57f3a86ae45a1dad0c5f1073.js" async></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/1.12.0/jquery.min.js'></script> 
    
<script type="text/javascript">
 
	function getCookie(name) {
      var value = "; " + document.cookie;
      var parts = value.split("; " + name + "=");
      if (parts.length == 2) return parts.pop().split(";").shift();
    }			
	
	<!--Start Sourcepoint Cookie Consent Code -->
    window._sp_ = window._sp_ || {};
    window._sp_.config = window._sp_.config || {};
    window._sp_.mms = window._sp_.mms || {};
    window._sp_.mms.cmd = window._sp_.mms.cmd || [];
    window._sp_.config.account_id = 374;
    // build consent url from current URL
    var consentDomain = "consent."+window.location.hostname.replace(/^www.?/g, "");
    window._sp_.config.mms_domain = consentDomain;
    window._sp_.config.cmp = _sp_.config.cmp || {};
    window._sp_.config.cmp.enabled = true;
    window._sp_.config.detection = _sp_.config.detection || {};
    window._sp_.config.detection.timeout = 1;
    //window._sp_.bootstrap('/wp-content/themes/the-resident/includes/js/messaging.js');
    window._sp_.mms.cmd.push(function(){ window._sp_.mms.startMsg(); });
 </script>
	
<!--URL in line below needs to be replaced with messaging file location on your CDN-->
<script type="text/javascript" src="wp-content/themes/the-resident/includes/js/messaging.js"></script>
<!--End Sourcepoint Cookie Consent Code -->

                        
<!-- Global Analytics Code: 'Index' wrapper -->
<script async src="//js-sec.indexww.com/ht/p/185246-80671361353614.js"></script>

  <!-- START DFP  -->
  <script async='async' src='https://www.googletagservices.com/tag/js/gpt.js'></script>
  <script>
      var googletag = googletag || {};
      googletag.cmd = googletag.cmd || [];
  </script>
  <script>
  
  //set breakpoints
    var displayWidth = window.innerWidth;
    var breakPointSm = 320;
    var breakPointMd = 728;
    var breakPointLg = 1024;

    var currentDisplay = "";

    if(displayWidth < breakPointMd) {
        var currentDisplay = "-mobile";
    } else if(displayWidth >= breakPointMd && displayWidth < breakPointLg) {
        var currentDisplay = "-tablet";
    } else {
        var currentDisplay = "-desktop";
    }
	
	
      // Production
      googletag.cmd.push(function() {
		// Category Targetting
          <?php 
		  $search = full_url();
		  $articlePost = "";
		  	if( strpos( $search, $GLOBALS['post']->post_name ) !== false) {
				//echo "\r\n var articlePage = 'true';\r\n"; 
				$articlePost = ",'".$GLOBALS['post']->post_name."'";
				
			}else{
				//echo "\r\n var articlePage = 'false';\r\n"; 
			}
			//echo "var articlePost = '".$articlePost."';\r\n";
            if($articlePage){
                    echo "googletag.pubads().setTargeting('category',[".$categoryTermsSlug.$articlePost."]);";
            }else{			  
                    echo "googletag.pubads().setTargeting('category',".(($category[0]->slug == '') ? "'homepage'" : $category[0]->slug ).");";
            }
           ?>

          if(displayWidth < breakPointMd) {
              googletag.pubads().setTargeting('device', 'mobile');
          } else if(displayWidth >= breakPointMd && displayWidth < breakPointLg) {
              googletag.pubads().setTargeting('device', 'tablet');
          } else {
              googletag.pubads().setTargeting('device', 'desktop');
          }

var leaderboardSizeMapping = googletag.sizeMapping().
        // Mobile
        addSize([breakPointSm, 200], [320, 50]).
        // Tablet
        addSize([breakPointMd, 200], [728, 90]).
        // Desktop
        addSize([breakPointLg, 200], [[728, 90], [970, 250]])
        // Build map
        .build();
		
          /*
          * Build DFP variable for The Resident site structure using post category from Wordpress
          **/
		 <?php  /*var $terms = get_terms( 'post_tag', array('hide_empty' => false,));*/ ?>				
          var sitecategory = '<?php $category = get_the_category(); echo $category[0]->slug ?>';
          // If we are on a category page get the full category and sub categories
          var pathname = window.location.pathname;
          if (pathname.includes('/category/')) { sitecategory = pathname.split("/category/").pop();
          };
          // If sitecategory is still empty we are on an unknown page or home page so check if we are at root otherwise use runofsite
          if (!sitecategory) { var is_root = location.pathname == "/";
                  if (is_root) { var sitecategory = "home"; }
                  else { var sitecategory = "runofsite"; }
          };

  
          /*
          * Define Ad Calls
          **/

          var adLb, adMpu1, adMpu2, adSticky, adWp;

          var initialLoad = [];

          // Now including sitecategory from above DFP Variable sitecategory
          // var dfpAdCall = '/327481562/archant/sw/' + sitecategory; //old version
		  
		  // var dfpNetworkId = '154725070';
		  // var dfpSite = 'sw';
		  // var dfpAdCall = '/' + dfpNetworkId + '/' + dfpSite + '/' + sitecategory;
		  
          var dfpAdCall = '/154725070/sw/' + sitecategory;
          //var dfpAdCall = '/154725070/test-archant/sw/'
	
		  // Wallpaper
		  adWp = googletag.defineSlot(dfpAdCall, [[1, 1]], "ad-slot-wp"+currentDisplay).addService(googletag.pubads());
		  initialLoad.push(adWp);
		  adWp.setTargeting('pos', ['wp']);
		  
		  // Leaderboard
		  adLb = googletag.defineSlot(dfpAdCall, [[320, 50]], "ad-slot-lb"+currentDisplay).addService(googletag.pubads()).defineSizeMapping(leaderboardSizeMapping);
		  initialLoad.push(adLb);
		  adLb.setTargeting('pos', ['lb']);
		  
		  // MPU1
          adMpu1 = googletag.defineSlot(dfpAdCall, [[300, 250], [300, 600]], "ad-slot-mpu-1"+currentDisplay).addService(googletag.pubads());
          initialLoad.push(adMpu1);
          adMpu1.setTargeting('pos', ['mpu1', 'mpu']);

          // MPU2
          adMpu2 = googletag.defineSlot(dfpAdCall, [[300, 250]], "ad-slot-mpu-2"+currentDisplay).addService(googletag.pubads());
          initialLoad.push(adMpu2);
          adMpu2.setTargeting('pos', ['mpu2', 'mpu']);

          // Sticky/Float
          adSticky = googletag.defineSlot(dfpAdCall, [[1, 1]], "ad-slot-float"+currentDisplay).addService(googletag.pubads());
          initialLoad.push(adSticky);
          adSticky.setTargeting('pos', ['sticky']);
	 

          googletag.pubads().enableSingleRequest();
          googletag.pubads().disableInitialLoad();
    
          googletag.pubads().collapseEmptyDivs();

          googletag.enableServices();
          
          //instantly load ads
          googletag.pubads().refresh(initialLoad, {changeCorrelator: false});
      });
  </script>
  <!-- END DFP -->
    
    <meta property="fb:pages" content="820195564673980">
    
    <!-- cat lennon google access -->
    <meta name="google-site-verification" content="VJCC1tz2Ztzeq8nAn2ovqfKwz_F7g-xuVs8XisQaI_g" />
    
</head>

<body <?php body_class('show-nothing'); ?>>
<div class="wrap"><div id="shifter">
<!--<div class="advert"><?php //dynamic_sidebar('header'); ?></div>-->

<div class="widget html"><center>
	<div class="advert"> 
		<div id="ad-slot-lb-desktop"> 
			  <script>
				if (window.innerWidth >= 1024) {
					googletag.cmd.push(function() { 
						googletag.display("ad-slot-lb-desktop"); 
					});
				}
			      </script> 
			  </div> 
			  <div id="ad-slot-lb-tablet"> 
			      <script>
				if (window.innerWidth >= 728 && window.innerWidth < 1024) {
					googletag.cmd.push(function() { 
						googletag.display("ad-slot-lb-tablet"); 
					});
				}
			      </script> 
			  </div> 
			  <div id="ad-slot-lb-mobile"> 
			      <script>
				if (window.innerWidth < 728) {
					googletag.cmd.push(function() { 
						googletag.display("ad-slot-lb-mobile"); 
					});
				}
			      </script> 
			  </div>
	</div>
</center>
</div>
<div class="header">
	<div class="header-holder">
		<div class="header-inner">

			<a href="/" id="logo" class="hidden"><?php bloginfo('name'); ?></a>
			<?php

			$upload_dir = wp_upload_dir();
			$cache_path = $upload_dir['basedir'].'/navigation.html';
			include $cache_path;

			?>
			<button id="show-search" onclick="highlightSearchField();"></button>
			<button id="show-menu"><span></span></button>
		</div>
	</div>
</div><!-- .header .header-inner -->
