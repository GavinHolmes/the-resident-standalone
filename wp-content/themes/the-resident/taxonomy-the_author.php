<?php get_header(); ?>

<div class="author_strip"><div class="author_strip_inner clear"><?php
	$the_author = get_term_by('slug',get_query_var('the_author'),'the_author');

	$author_image_id = get_term_meta($the_author->term_id,'tr-author-image-id',true);
	$thumbnail = wp_get_attachment_image_src( $author_image_id , 'tr-main' );

	if($thumbnail) echo '<div class="author_image" style="background-image: url('.$thumbnail[0].')"></div>';
	else  echo '<div class="author_image"></div>';
	
	echo '<h2>'.$the_author->name.' <span>| Author</span></h2>';
	echo '<p>'.$the_author->description.'</p>';

?></div></div>

<div class="content">

	<div class="left">
		<div class="author_box">Viewing only <strong><?php echo $the_author->name; ?>’s</strong> articles</div>
	<?php
	if ( have_posts() ) while ( have_posts() ) {
		the_post(); ?>

		<div class="author_post clear">
			<div class="date"><span><?php echo the_time('M'); ?></span><?php echo the_time('j'); ?></div>
			<a href="<?php the_permalink(); ?>">
				<h4><?php echo $ad_feature.get_the_title(); ?></h4>
				<?php the_excerpt() ?>
			</a>
		</div>

		<?php
	} else echo '<p>no posts</p>';

	echo pagination();

	?>
	</div>

<?php get_sidebar(); ?>

</div><!-- .content -->

<?php get_footer(); ?>
