<?php

if( in_array(get_queried_object_id(),array(235,254,256,257,361,353,359,358,355))) add_filter( 'body_class', 'tr_add_platinum_body_class');
function tr_add_platinum_body_class( $classes ) {
	return array_merge( $classes, array( 'is_platinum' ) );
}

get_header(); ?>

<div id="title-strip">
	<h2><?php single_cat_title(); ?></h2>
</div>

<div class="content">

	<div class="left clear">
	<?php
	if ( have_posts() ) while ( have_posts() ) {
		the_post();
		if(get_post_meta($post->ID,'is_ad_feature',true) == 1) $ad_feature = '<strong class="ad_feature">Ad Feature</strong> ';
		else $ad_feature = '';
		?>

		<a href="<?php the_permalink(); ?>" class="single_post">
			<?php
				if(has_post_thumbnail()) the_post_thumbnail('tr-main');
				else echo '<img src="'.get_stylesheet_directory_uri().'/includes/img/no-image-600x400.png" alt="No Image" />';
			 ?>
			<h4><?php echo $ad_feature.get_the_title(); ?></h4>
			<?php the_excerpt() ?>
		</a>

		<?php
	} else echo '<p>no posts</p>';

	echo pagination();

	?>
	</div>

<?php get_sidebar(); ?>

</div><!-- .content -->

<?php get_footer(); ?>
