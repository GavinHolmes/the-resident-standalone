<?php
/**
 * Template Name: Recipe Migration
 */

set_time_limit(600);

get_header(); ?>

 <div class="left">
 <?php

 global $wpdb;
 $recipes = $wpdb->get_results('SELECT * FROM `wp_10_yumprint_recipe_recipe`');

//var_dump($recipes);

foreach($recipes as $recipe) {

	$recipe->recipe = json_decode($recipe->recipe);

	if( isset($recipe->recipe->ingredients[0]->lines) ) foreach($recipe->recipe->ingredients[0]->lines as $line) $recipe->ingredients .= $line.PHP_EOL;
	if( isset($recipe->recipe->directions[0]->lines) ) foreach($recipe->recipe->directions[0]->lines as $line) $recipe->directions .= $line.PHP_EOL;
	if( isset($recipe->recipe->notes[0]->lines) ) foreach($recipe->recipe->notes[0]->lines as $line) $recipe->notes .= $line.PHP_EOL;

	if( !empty($recipe->recipe->image) ) {
		//$recipe->recipe->image = str_replace('/wp-content/uploads/sites/10/','/wp-content/uploads/sites/2/',$recipe->recipe->image);
		$recipe->attachment_id = $wpdb->get_var( "SELECT `ID` FROM $wpdb->posts WHERE guid = '".$recipe->recipe->image."'" );
	}

	//var_dump($recipe);


	$recipe_id = wp_insert_post(array(
		'post_title' => $recipe->recipe->title,
		'post_date' => $recipe->created,
		'post_status' => 'publish',
		'post_type' => 'recipe',
	));

	update_post_meta($recipe_id, 'author', $recipe->recipe->author );
	update_post_meta($recipe_id, 'prepTime', $recipe->recipe->prepTime );
	update_post_meta($recipe_id, 'cookTime', $recipe->recipe->cookTime );
	update_post_meta($recipe_id, 'totalTime', $recipe->recipe->totalTime );
	update_post_meta($recipe_id, 'servings', $recipe->recipe->servings );
	update_post_meta($recipe_id, 'adapted', $recipe->recipe->adapted );
	update_post_meta($recipe_id, 'adaptedLink', $recipe->recipe->adaptedLink );
	update_post_meta($recipe_id, 'summary', $recipe->recipe->summary );
	update_post_meta($recipe_id, 'yields', $recipe->recipe->yields );
	update_post_meta($recipe_id, 'ingredients', $recipe->ingredients );
	update_post_meta($recipe_id, 'directions', $recipe->directions );
	update_post_meta($recipe_id, 'notes', $recipe->notes );
	if( isset($recipe->attachment_id) and !empty($recipe->attachment_id) ) set_post_thumbnail($recipe_id, $recipe->attachment_id);

	echo '<br/>--------<br/>';
	var_dump($recipe->id);
	var_dump($recipe->post_id);
	echo '<br/>--------<br/>';


	$post = get_post($recipe->post_id);

	$pattern = '\[(\[?)(yumprint\-recipe)(?![\w-])([^\]\/]*(?:\/(?!\])[^\]\/]*)*?)(?:(\/)\]|\](?:([^\[]*+(?:\[(?!\/\2\])[^\[]*+)*+)\[\/\2\])?)(\]?)';

	if (   preg_match_all( '/'. $pattern .'/s', $post->post_content, $matches )
			&& array_key_exists( 2, $matches )
			&& in_array( 'yumprint-recipe', $matches[2] )
			){

			if(is_array($matches[0])){
				wp_update_post(array(
						 'ID'           => $post->ID,
						 'post_content' => str_replace("[yumprint-recipe id='".$recipe->id."']", "[recipe id=".$recipe_id."]", $post->post_content)
				 ));
			}
	}

	//if((int)$i++ > 2) break;

}




 ?>
 </div>

 <?php get_footer(); ?>
