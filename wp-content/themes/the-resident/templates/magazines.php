<?php
/**
 * Template Name: Magazines
 */

 get_header(); ?>

 <div id="title-strip">
 	<h2><?php the_title(); ?></h2>
 </div>

 <div class="content">

 	<div class="left">
 	<?php
 	if ( have_posts() ) while ( have_posts() ) {
 		the_post();
 		the_content();
 	}

	$magazines = array(
		'The Resident' => array(
			'pagesuite_url' => 'https://edition.pagesuite-professional.co.uk//launch.aspx?pbid=bb2fb5b8-6d8a-4b00-9277-8a56a7f97724',
			'image' => 'https://edition.pagesuite-professional.co.uk//get_image.aspx?w=220&amp;pbid=bb2fb5b8-6d8a-4b00-9277-8a56a7f97724',
			'twitter_url' => 'https://twitter.com/theresidentmag'
		),
		'The Hill Resident' => array(
			'pagesuite_url' => 'https://edition.pagesuite-professional.co.uk/Launch.aspx?PBID=291e8256-5629-4e74-80b3-389c1d08eab9',
			'image' => 'https://edition.pagesuite-professional.co.uk/get_image.aspx?w=220&pbid=291e8256-5629-4e74-80b3-389c1d08eab9',
			'twitter_url' => 'https://twitter.com/thehillmag'
		),
		'SW Resident' => array(
			'pagesuite_url' => 'https://edition.pagesuite-professional.co.uk/Launch.aspx?PBID=8d4d826e-6438-4d94-a218-da30b864e3e5',
			'image' => 'https://edition.pagesuite-professional.co.uk/get_image.aspx?w=220&amp;pbid=8d4d826e-6438-4d94-a218-da30b864e3e5',
			'twitter_url' => 'https://twitter.com/SWmagazine'
		),
		'The Guide Resident' => array(
			'pagesuite_url' => 'https://edition.pagesuite-professional.co.uk/Launch.aspx?PBID=6b18c2f8-4744-4c89-8f4e-69267cc84576',
			'image' => 'https://edition.pagesuite-professional.co.uk/get_image.aspx?w=220&pbid=6b18c2f8-4744-4c89-8f4e-69267cc84576',
			'twitter_url' => 'https://twitter.com/theguidemag'
		),
		'Living South Resident' => array(
			'pagesuite_url' => 'https://edition.pagesuite-professional.co.uk/Launch.aspx?PBID=afe201bc-f9f9-44ef-b1b2-22fc265981ec',
			'image' => 'https://edition.pagesuite-professional.co.uk//get_image.aspx?w=220&pbid=afe201bc-f9f9-44ef-b1b2-22fc265981ec',
			'twitter_url' => 'https://twitter.com/livingsouth'
		),
		'School Report' => array(
			'pagesuite_url' => 'https://edition.pagesuite-professional.co.uk//launch.aspx?eid=68fffe6f-f3d6-47f0-9e35-d6f575f74aeb',
			'image' => 'https://edition.pagesuite-professional.co.uk/get_image.aspx?w=220&pbid=e5eb0f29-d537-4069-8e43-5b7cb3a36c98',
			'twitter_url' => 'https://twitter.com/SchoolReportMag'
		),
	);

	$magazines_archive = array(
		'Richmond & Barnes Resident' => array(
			'pagesuite_url' => 'https://edition.pagesuite-professional.co.uk/Launch.aspx?PBID=cfbd91eb-7a0d-4447-a335-7d1dc3e5cbd8',
			'image' => 'https://edition.pagesuite-professional.co.uk/get_image.aspx?w=220&pbid=cfbd91eb-7a0d-4447-a335-7d1dc3e5cbd8',
			'twitter_url' => 'https://twitter.com/Richmond_Res'
		),
		'The Angel Resident' => array(
			'pagesuite_url' => 'https://edition.pagesuite-professional.co.uk/Launch.aspx?PBID=cb3299b8-4964-4173-9916-7a3b855e2918',
			'image' => 'https://edition.pagesuite-professional.co.uk/get_image.aspx?w=220&pbid=cb3299b8-4964-4173-9916-7a3b855e2918',
			'twitter_url' => 'https://twitter.com/angelmag'
		),
		'Westside Resident' => array(
			'pagesuite_url' => 'https://edition.pagesuite-professional.co.uk/Launch.aspx?PBID=9ff68b3e-21fe-4b77-b6b2-de5da4149483',
			'twitter_url' => 'https://twitter.com/westsidemags',
			'image' => 'https://edition.pagesuite-professional.co.uk/get_image.aspx?w=220&pbid=9ff68b3e-21fe-4b77-b6b2-de5da4149483'
		),
		'Resident @Home' => array(
			'pagesuite_url' => 'https://edition.pagesuite-professional.co.uk//launch.aspx?pbid=324cedd3-394b-487b-b4fa-3576e481130a',
			'image' => 'https://edition.pagesuite-professional.co.uk/get_image.aspx?w=220&pbid=324cedd3-394b-487b-b4fa-3576e481130a',
		),
		'Platinum Resident' => array(
			'pagesuite_url' => 'https://edition.pagesuite-professional.co.uk//launch.aspx?pbid=1f1d2ca6-3d74-446c-b8ab-5c9c4225511f',
			'image' => 'https://edition.pagesuite-professional.co.uk/get_image.aspx?w=220&pbid=1f1d2ca6-3d74-446c-b8ab-5c9c4225511f',
			'twitter_url' => ''
		),
		'Prime' => array(
			'pagesuite_url' => 'https://edition.pagesuite-professional.co.uk/launch.aspx?pbid=e738fa60-546b-44c6-892b-5eae91fce890',
			'twitter_url' => 'https://twitter.com/Primeinter',
			'image' => 'https://theresident.wpms.greatbritishlife.co.uk/wp-content/uploads/sites/10/2017/05/PRIME.jpg'
		),
		'Finance & Money' => array(
			'pagesuite_url' => 'https://edition.pagesuite-professional.co.uk//launch.aspx?pbid=77475c6e-a0c9-404a-94e2-55453f0dd07b',
			'image' => 'https://edition.pagesuite-professional.co.uk/get_image.aspx?w=220&pbid=77475c6e-a0c9-404a-94e2-55453f0dd07b'
		),
		'Wedding Inspiration SW' => array(
			'pagesuite_url' => 'https://edition.pagesuite-professional.co.uk/launch.aspx?eid=8c2d4f8a-66fc-40ca-a506-208b0ebad4d6',
			'image' => 'https://edition.pagesuite-professional.co.uk/get_image.aspx?w=220&pbid=39b76a09-a215-4a88-9d5f-f22e3c4cf214'
		),
		'Wedding Inspiration SE' => array(
			'pagesuite_url' => 'https://edition.pagesuite-professional.co.uk/launch.aspx?eid=8c2d4f8a-66fc-40ca-a506-208b0ebad4d6',
			'image' => 'https://edition.pagesuite-professional.co.uk/get_image.aspx?w=220&pbid=39b76a09-a215-4a88-9d5f-f22e3c4cf214'
		)
	);
 	?>

		<div id="magazine-list" class="clear">
			<?php foreach($magazines as $title => $mag){ ?>
				<div class="mag">
					<a href="<?php echo $mag['pagesuite_url']; ?>"><?php echo $mag['image'] ? '<img src="'.$mag['image'].'" alt="">' : 'No image'; ?></a>
					<h2>
						<?php
						echo $title;
						if( isset($mag['pagesuite_url']) ) echo '<a href="' . $mag['pagesuite_url'] . '" target="_blank">Read online</a>';
						if( isset($mag['buyamag_url']) ) echo '<a href="' . $mag['buyamag_url'] . '" target="_blank">Buy online</a>';
						?>
						<a href="<?php echo $mag['twitter_url']; ?>" class=" twitter" target="_blank">Twitter</a>
				</h2>
				</div>
			<?php } ?>

		</div>
		<br></br>
		<h2>Archive Publications</h2>
		<br></br>
		<div id="magazine-list" class="clear">
			<?php foreach($magazines_archive as $title => $mag){ ?>
				<div class="mag">
					<a href="<?php echo $mag['pagesuite_url']; ?>"><?php echo $mag['image'] ? '<img src="'.$mag['image'].'" alt="">' : 'No image'; ?></a>
					
					<h2>
						<?php
						echo $title;
						if( isset($mag['pagesuite_url']) ) echo '<a href="' . $mag['pagesuite_url'] . '" target="_blank">Read online</a>';
						if( isset($mag['buyamag_url']) ) echo '<a href="' . $mag['buyamag_url'] . '" target="_blank">Buy online</a>';
						?>
						<a href="<?php echo $mag['twitter_url']; ?>" class=" twitter" target="_blank">Twitter</a>
				</h2>
				</div>
			<?php } ?>

		</div>

 	</div>

 <?php get_sidebar(); ?>

 </div><!-- .content -->

 <?php get_footer(); ?>
