<?php
/**
 * Template Name: Taxonomy SEO Migration
 */

 set_time_limit(300);

$titles = get_option('seo_ultimate_module_titles');
foreach($titles['taxonomy_titles'] as $id => $title){
	update_term_meta($id, 'seo_title', $title);
	//echo $id .', '. $title.'<br/>';
}

$descriptions = get_option('seo_ultimate_module_meta');
foreach($descriptions['taxonomy_descriptions'] as $id => $description){
	update_term_meta($id, 'seo_description', $description);
	//echo $id .', '. $description.'<br/>';
}

