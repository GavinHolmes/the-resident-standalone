<?php get_header(); ?>

<div id="title-strip">
	<h2>Search</h2>
</div>

<div class="content">

	<div class="left">

		<div class="search_box">
			<?php the_widget( 'tr_search_form', array('show' => true) ); ?>
			<p>Found <?php echo $wp_query->found_posts; ?> results for <strong>'<?php echo get_search_query(); ?>'</strong> within the site</p>
		</div>

		<?php if ( have_posts() ) while ( have_posts() ) {
			the_post();
			echo '<div class="found_post clear">';
			if(has_post_thumbnail()) the_post_thumbnail('tr-main');
			else echo '<img src="'.get_stylesheet_directory_uri().'/includes/img/no-image-600x400.png" alt="No Image" />'; ?>

			<div class="post_details">
				<div class="terms"><?php
					$is_ad_feature = get_post_meta($post->ID,'is_ad_feature',true);
					if($is_ad_feature) echo '<span class="is_ad_feature">AD FEATURE</span> ';

					$cats = get_the_terms($post->ID, 'category');
					if($cats) foreach($cats as $cat) echo '<a class="category" href="'.get_term_link($cat).'">'.$cat->name.'</a> ';
				?></div>

				<a href="<?php the_permalink(); ?>">
					<h4><?php echo $ad_feature.get_the_title(); ?></h4>
					<?php the_excerpt() ?>
				</a>
			</div></div><!-- .found_post -->

		<?php } 

		echo pagination();

		?>
	</div>

	<?php get_sidebar(); ?>

</div><!-- .content -->

<?php get_footer(); ?>
